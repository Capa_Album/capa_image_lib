#include "test_helper.h"
#include "capa_image_lib.h"
#include <iostream>

using namespace capa;

bool any_error(error_object const &err, error_object const &err2) {

    rapidjson::Document dom;
    rapidjson::Document dom2;
    if (err.fail())
        std::cout << get_json_text(get_error_json(err, dom, dom.GetAllocator())) << std::endl;

    if (err2.fail())
        std::cout << get_json_text(get_error_json(err2, dom2, dom2.GetAllocator())) << std::endl;
    return err.fail() || err2.fail();
}

bool test_create_thumbnail_from_file_jpg(std::string const &in_file_name, std::string const &out_file_name) {

    std::stringstream in_memory(std::ios_base::in | std::ios_base::out | std::ios_base::binary);
    error_object err;
    error_object err2;

    create_thumbnail_jpg_IFOB(in_file_name, in_memory, 8, 400, 400, err);
    create_thumbnail_jpg_IFOF(in_file_name, out_file_name, 8, 400, 400, err2);

    if (!any_error(err, err2)) {
        std::ifstream in_file;
        in_file.open(out_file_name, std::ifstream::in | std::ios_base::binary);

        std::istream_iterator<char> iter_file(in_file);
        std::istream_iterator<char> iter_buffer(in_memory);
        std::istream_iterator<char> iter_buffer_end;

        return std::equal(iter_buffer, iter_buffer_end, iter_file) == true;
    } else {
        return false;
    }

}

bool test_create_thumbnail_from_buffer_jpg(std::string const &in_file_name) {

    std::stringstream out_memory(std::ios_base::in | std::ios_base::out | std::ios_base::binary);
    std::stringstream out_memory2(std::ios_base::in | std::ios_base::out | std::ios_base::binary);
    error_object err;

    std::ifstream in_file;
    in_file.open(in_file_name, std::ifstream::in | std::ios_base::binary);

    create_thumbnail_IBOB(in_file, out_memory, 1, 400, 400, err);
    if (err.fail()) {
        std::cout << err.function_name << " " << err.err_description << std::endl;
        return false;
    }

    create_thumbnail_IFOB(in_file_name, out_memory2, 1, 400, 400, err);
    if (err.fail()) {
        std::cout << err.function_name << " " << err.err_description << std::endl;
        return false;
    }


    std::istream_iterator<char> iter_buffer(out_memory);
    std::istream_iterator<char> iter_buffer2(out_memory2);
    std::istream_iterator<char> iter_buffer_end;

    return std::equal(iter_buffer, iter_buffer_end, iter_buffer2) == true;
}

bool test_create_thumbnail_from_buffer_png(std::string const &in_file_name) {

    std::stringstream out_memory(std::ios_base::in | std::ios_base::out | std::ios_base::binary);
    std::stringstream out_memory2(std::ios_base::in | std::ios_base::out | std::ios_base::binary);
    error_object err;

    std::ifstream in_file;
    in_file.open(in_file_name, std::ifstream::in | std::ios_base::binary);

    create_thumbnail_IBOB(in_file, out_memory, 1, 400, 400, err);
    if (err.fail()) {
        std::cout << err.function_name << " " << err.err_description << std::endl;
        return false;
    }

    create_thumbnail_IFOB(in_file_name, out_memory2, 1, 400, 400, err);
    if (err.fail()) {
        std::cout << err.function_name << " " << err.err_description << std::endl;
        return false;
    }

    std::istream_iterator<char> iter_buffer(out_memory);
    std::istream_iterator<char> iter_buffer2(out_memory2);
    std::istream_iterator<char> iter_buffer_end;

    return std::equal(iter_buffer, iter_buffer_end, iter_buffer2) == true;
}

bool test_create_thumbnail_from_file_png(std::string const &in_file_name, std::string const &out_file_name) {

    std::stringstream in_memory(std::ios_base::in | std::ios_base::out | std::ios_base::binary);
    error_object err;

    create_thumbnail_IFOB(in_file_name, in_memory, 2, 400, 400, err);
    create_thumbnail_IFOF(in_file_name, out_file_name, 2, 400, 400, err);

    std::ifstream in_file;
    in_file.open(out_file_name, std::ifstream::in | std::ios_base::binary);

    std::istream_iterator<char> iter_file(in_file);
    std::istream_iterator<char> iter_buffer(in_memory);
    std::istream_iterator<char> iter_buffer_end;

    return std::equal(iter_buffer, iter_buffer_end, iter_file) == true;

}

bool test_resize(std::string const &in_file_name, std::string const &out_file_name) {
    std::stringstream out_memory(std::ios_base::out | std::ios_base::binary);
    error_object err;

    resize_image_IFOB(in_file_name, out_memory, 400, 400, err);
    resize_image_IFOF(in_file_name, out_file_name, 400, 400, err);

    std::ifstream in_file_original;
    in_file_original.open(in_file_name, std::ifstream::in | std::ios_base::binary);

    std::stringstream out_memory2(std::ios_base::out | std::ios_base::binary);

    resize_image_IBOB(in_file_original, out_memory2, 400, 400, err);

    std::ifstream in_file;
    in_file.open(out_file_name, std::ifstream::in | std::ios_base::binary);

    std::istream_iterator<char> iter_file(in_file);

    std::istream_iterator<char> iter_buffer(out_memory);
    std::istream_iterator<char> iter_buffer_end;

    std::istream_iterator<char> iter_buffer2(out_memory2);


    return std::equal(iter_buffer, iter_buffer_end, iter_buffer2) &&
           std::equal(iter_buffer, iter_buffer_end, iter_file) == true;
}

bool test_exif_from_buffer(std::string const &in_file_name) {
    rapidjson::Document val1;
    rapidjson::Document val2;


    std::ifstream in_file;
    in_file.open(in_file_name, std::ifstream::in | std::ios_base::binary);

    extract_exif_jpg_IB(in_file, val1, val1.GetAllocator());
    extract_exif_jpg_IF(in_file_name, val2, val2.GetAllocator());

    //std::cout << "original: " << get_json_text(val1) << std::endl ;

    auto image_width_node = rapidjson::Pointer("/EXIF/ImageWidth").Get(val1);
    auto image_height_node = rapidjson::Pointer("/EXIF/ImageHeight").Get(val1);

//    std::cout << "xpath: "
//              << " width: " << image_width_node->GetInt()
//              << " height: " << image_height_node->GetInt() <<std::endl;
//
//    std::cout << "original: " << get_json_text(val2) << std::endl ;

    auto image_width_node_2 = rapidjson::Pointer("/EXIF/ImageWidth").Get(val2);
    auto image_height_node_2 = rapidjson::Pointer("/EXIF/ImageHeight").Get(val2);

//    std::cout << "xpath: "
//              << " width: " << image_width_node_2->GetInt()
//              << " height: " << image_height_node_2->GetInt() <<std::endl;

    return image_height_node->GetInt() == image_height_node_2->GetInt()
           && image_width_node->GetInt() == image_width_node_2->GetInt();


}

bool test_get_info(std::string const &in_file_name) {
    rapidjson::Document val1;
    rapidjson::Document val2;
    extract_exif_jpg_IF(in_file_name, val1, val1.GetAllocator());
    extract_picture_info(in_file_name, val2, val2.GetAllocator());

    //std::cout << "original: " << get_json_text(val1) << std::endl ;

    auto image_width_node = rapidjson::Pointer("/EXIF/ImageWidth").Get(val1);
    auto image_height_node = rapidjson::Pointer("/EXIF/ImageHeight").Get(val1);

//    std::cout << "xpath: "
//              << " width: " << image_width_node->GetInt()
//              << " height: " << image_height_node->GetInt() <<std::endl;
//
//    std::cout << "original: " << get_json_text(val2) << std::endl ;

    auto image_width_node_2 = rapidjson::Pointer("/EXIF/ImageWidth").Get(val2);
    auto image_height_node_2 = rapidjson::Pointer("/EXIF/ImageHeight").Get(val2);

//    std::cout << "xpath: "
//              << " width: " << image_width_node_2->GetInt()
//              << " height: " << image_height_node_2->GetInt() <<std::endl;

    return image_height_node->GetInt() == image_height_node_2->GetInt()
           && image_width_node->GetInt() == image_width_node_2->GetInt();

}

bool test_get_info_array(std::vector<std::string> const &in_arr_file_name) {
    rapidjson::Document val1;
    rapidjson::Document val2;

    extract_exif_jpg_IF(in_arr_file_name, val1, val1.GetAllocator());
    extract_picture_info(in_arr_file_name, val2, val2.GetAllocator());

//    std::cout << "original: " << val1.Size() << std::endl ;
//    std::cout << "original: " << val2.Size() << std::endl ;

    //One picture doesn't have EXIF data
    //So exif extraction will fail
    //But picture info will succeed
    return val2.Size() - val1.Size() == 0;

}