FROM buildpack-deps:buster

WORKDIR /CIL

ADD src /CIL/src
ADD test /CIL/test
ADD CMakeLists.txt /CIL/CMakeLists.txt
ADD build.sh /CIL/build.sh

RUN apt-get update -y
RUN apt-get install -y cmake libjpeg-dev libpng-dev

CMD [ "/CIL/build.sh" ]