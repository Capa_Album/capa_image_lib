Breakdown of the Methods names:
--------------------------

<Act_ion>_\<FileType>_\<IDevice>\<ODevice>\_async

Act_ion - the function name: 

* create_thumbnail
* resize_image
* get_exif

FileType - If present, this function can only work on files or buffers that match the file type.
If not present then the function will try to deduce the file type.

Supported file types:
* jpg
* png
* more to come

IDevice - The Input Device:
* Buffer
* File

ODevice - The Output Device:
* Buffer
* File

async:
* most functions have a synchronous or async (promise) interface.

| Function | Act_ion | FileType | IDevice ODevice | async? |
|--------|---------|-----------|--------------------|-----|
|resize_image_IFOB|resize_image|generic|IFOB|   
|create_thumbnail_png_IBOB|create_thumbnail|png|IBOB|
|create_thumbnail_png_IBOB_async|create_thumbnail|png|IBOB|async|


Methods:
------------

* get_exif     -   receives an array of file names and returns the exifs in a json form

* resize_image_IFOF       -   

| Argument          | description | type |
|-------------------|:-----------:|:-----:|
|original_file_name |source file name |string|
|resized_file_name  |destination file name|string|
|w              | destination file width| int|
|h  | destination file height|int|

note: the function does not create the path!


* resize_image_IFOB       -   return a buffer of the resized picture.
The returned buffer is in the format of PNG image file format!

| Argument          | description | type |
|-------------------|:-----------:|:-----:|
|original_file_name |source file name |string|
|w              | destination file width| int|
|h  | destination file height|int|


* create_thumbnail_IFOF - receives an image filename and creates a thumbnail file
* create_thumbnail_jpg_IFOF
* create_thumbnail_png_IFOF   
* create_thumbnail_IFOF_async

| Argument          | description | type |
|-------------------|:-----------:|:-----:|
|original_file_name |source file name |string|
|resized_file_name  |destination file name|string|
|orientation|<http://www.daveperrett.com/articles/2012/07/28/exif-orientation-handling-is-a-ghetto/>|int|
| |taken from the exif orientation field|
|w              | destination file width| int|
|h  | destination file height|int|

note: the function does not create the path!

* create_thumbnail_IFOB - receives an image filename and returns the thumbnail as a buffer
* create_thumbnail_png_IFOB
* create_thumbnail_jpg_IFOB
* create_thumbnail_jpg_IFOB_async

| Argument          | description | type |
|-------------------|:-----------:|:-----:|
|original_file_name |source file name |string|
|orientation|<http://www.daveperrett.com/articles/2012/07/28/exif-orientation-handling-is-a-ghetto/>|int|
| |taken from the exif orientation field|
|w              | destination file width| int|
|h  | destination file height|int|

* create_thumbnail_IBOB - receives an image as buffer and returns the thumbnail as a buffer
* create_thumbnail_png_IBOB
* create_thumbnail_jpg_IBOB
* create_thumbnail_jpg_IBOB_async

| Argument          | description | type |
|-------------------|:-----------:|:-----:|
|original_buffer | buffer of the image |string|
|orientation|<http://www.daveperrett.com/articles/2012/07/28/exif-orientation-handling-is-a-ghetto/>|int|
| |taken from the exif orientation field|
|w              | destination file width| int|
|h  | destination file height|int|

* get_picture_info - Get information about the picture such as Width and Height.
This function should be used as a fallback function when the image has no EXIF data.
