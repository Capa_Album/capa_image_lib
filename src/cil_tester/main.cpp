#include "test_helper.h"

#define BOOST_TEST_MODULE CIL Test

#include <boost/test/included/unit_test.hpp>

using namespace boost::unit_test;

#define OUT_IMAGE_JPG IMAGES_ROOT R"(/test/out_images/test_out.jpg)"
#define OUT_THUMB_JPG IMAGES_ROOT R"(/test/out_images/thumb_out.jpg)"
#define OUT_IMAGE_PNG IMAGES_ROOT R"(/test/out_images/test_out.png)"
#define OUT_THUMB_PNG IMAGES_ROOT R"(/test/out_images/thumb_out.png)"

#define IN_IMAGE_JPG IMAGES_ROOT R"(/test/images/test.jpg)"
#define IN_IMAGE_PNG IMAGES_ROOT R"(/test/images/test.png)"

#define IN_IMAGE_ARRAY_JPG {IMAGES_ROOT R"(/test/images/test.jpg)",  \
                            IMAGES_ROOT R"(/test/images/test.jpg)",  \
                            IMAGES_ROOT R"(/test/images/test.jpg)"  }

BOOST_AUTO_TEST_CASE(test_create_thumbnail, *description("Testing create thumbnail")) {
    BOOST_TEST(test_create_thumbnail_from_file_jpg(IN_IMAGE_JPG, OUT_THUMB_JPG));
    BOOST_TEST(test_create_thumbnail_from_file_png(IN_IMAGE_PNG, OUT_THUMB_PNG));
}

//this suite tests the error mechanism ... and it's very confusing so I disabled it
//BOOST_AUTO_TEST_CASE(test_errors, *description("Testing errors")) {
//    BOOST_TEST(!test_create_thumbnail_from_file_jpg(IN_IMAGE_PNG, OUT_THUMB_PNG));
//}

BOOST_AUTO_TEST_CASE(test_create_thumbnail_from_buffer, *description("Testing create thumbnail")) {
    BOOST_TEST(test_create_thumbnail_from_buffer_jpg(IN_IMAGE_JPG));
    BOOST_TEST(test_create_thumbnail_from_buffer_png(IN_IMAGE_PNG));
}

BOOST_AUTO_TEST_CASE(test_images_resize, *description("Testing resize image")) {
    BOOST_TEST(test_resize(IN_IMAGE_JPG, OUT_IMAGE_JPG));
    BOOST_TEST(test_resize(IN_IMAGE_PNG, OUT_IMAGE_PNG));
}

BOOST_AUTO_TEST_CASE(test_exif, *description("Testing get EXIF and info")) {
    BOOST_TEST(test_exif_from_buffer(IN_IMAGE_JPG));
    BOOST_TEST(test_get_info(IN_IMAGE_JPG));
    BOOST_TEST(test_get_info_array(IN_IMAGE_ARRAY_JPG));
}
