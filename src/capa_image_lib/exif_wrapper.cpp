//
// Created by manicqin on 23/10/17.
//

#include <vector>
#include "exif_wrapper.h"
#include "utils.h"

#undef BOOST_GIL_EXTENSION_IO_PNG_C_LIB_COMPILED_AS_CPLUSPLUS
#undef BOOST_GIL_EXTENSION_IO_JPEG_C_LIB_COMPILED_AS_CPLUSPLUS

#include "boost/gil.hpp"
#include "boost/gil/image.hpp"
#include "boost/gil/typedefs.hpp"
#include "boost/gil/extension/io/jpeg.hpp"
#include "boost/gil/extension/io/png.hpp"
#include "boost/gil/extension/numeric/sampler.hpp"
#include "boost/gil/extension/numeric/resample.hpp"


void get_json(easyexif::EXIFInfo::Geolocation_t::Coord_t const &coords, rapidjson::Value &obj, rapidjson::Document::AllocatorType &alloc) {
    using namespace rapidjson;
    if(!obj.IsObject())
        obj.SetObject();

    obj.AddMember("degrees",
                  Value(kNumberType).SetDouble(coords.degrees),
                  alloc);

    obj.AddMember("minutes",
                  Value(kNumberType).SetDouble(coords.minutes),
                  alloc);

    obj.AddMember("seconds",
                  Value(kNumberType).SetDouble(coords.seconds),
                  alloc);

    obj.AddMember("direction",
                  Value(kNumberType).SetUint(coords.direction),
                  alloc);
}

void get_json(easyexif::EXIFInfo const &exifInfo, rapidjson::Value &doc, rapidjson::Document::AllocatorType &allocator) {
    using namespace rapidjson;

    if(!doc.IsObject())
        doc.SetObject();

    Value obj(rapidjson::kObjectType);

    obj.AddMember("ImageDescription",
                  Value(kStringType).Set(exifInfo.ImageDescription,allocator),
                  allocator);

    obj.AddMember("Make",
                  Value(kStringType).Set(exifInfo.Make,allocator),
                  allocator);

    obj.AddMember("Model",
                  Value(kStringType).Set(exifInfo.Model,allocator),
                  allocator);

    obj.AddMember("Software",
                  Value(kStringType).Set(exifInfo.Software,allocator),
                  allocator);

    obj.AddMember("DateTime",
                  Value(kStringType).Set(exifInfo.DateTime,allocator),
                  allocator);

    obj.AddMember("DateTimeOriginal",
                  Value(kStringType).Set(exifInfo.DateTimeOriginal,allocator),
                  allocator);

    obj.AddMember("DateTimeDigitized",
                  Value(kStringType).Set(exifInfo.DateTimeDigitized,allocator),
                  allocator);

    obj.AddMember("SubSecTimeOriginal",
                  Value(kStringType).Set(exifInfo.SubSecTimeOriginal,allocator),
                  allocator);

    obj.AddMember("Copyright",
                  Value(kStringType).Set(exifInfo.Copyright,allocator),
                  allocator);

    // Shorts / unsigned / double

    obj.AddMember("ByteAlign",
                  Value(kNumberType).SetInt(exifInfo.ByteAlign),
                  allocator);

    obj.AddMember("Orientation",
                  Value(kNumberType).SetUint(exifInfo.Orientation),
                  allocator);

    obj.AddMember("BitsPerSample",
                  Value(kNumberType).SetUint(exifInfo.BitsPerSample),
                  allocator);

    obj.AddMember("ExposureTime",
                  Value(kNumberType).SetDouble(exifInfo.ExposureTime),
                  allocator);

    obj.AddMember("FNumber",
                  Value(kNumberType).SetDouble(exifInfo.FNumber),
                  allocator);

    obj.AddMember("ISOSpeedRatings",
                  Value(kNumberType).SetUint(exifInfo.ISOSpeedRatings),
                  allocator);

    obj.AddMember("ShutterSpeedValue",
                  Value(kNumberType).SetDouble(exifInfo.ShutterSpeedValue),
                  allocator);

    obj.AddMember("ExposureBiasValue",
                  Value(kNumberType).SetDouble(exifInfo.ExposureBiasValue),
                  allocator);

    obj.AddMember("SubjectDistance",
                  Value(kNumberType).SetDouble(exifInfo.SubjectDistance),
                  allocator);

    obj.AddMember("FocalLength",
                  Value(kNumberType).SetDouble(exifInfo.FocalLength),
                  allocator);


    obj.AddMember("FocalLengthIn35mm",
                  Value(kNumberType).SetUint(exifInfo.FocalLengthIn35mm),
                  allocator);

    obj.AddMember("Flash",
                  Value(kNumberType).SetInt(exifInfo.Flash),
                  allocator);

    obj.AddMember("MeteringMode",
                  Value(kNumberType).SetUint(exifInfo.MeteringMode),
                  allocator);


    obj.AddMember("ImageWidth",
                  Value(kNumberType).SetUint(exifInfo.ImageWidth),
                  allocator);

    obj.AddMember("ImageHeight",
                  Value(kNumberType).SetUint(exifInfo.ImageHeight),
                  allocator);

    get_json(exifInfo.GeoLocation,obj,allocator);
    get_json(exifInfo.LensInfo,obj,allocator);
    doc.AddMember("EXIF",obj,allocator);
}

void get_json(easyexif::EXIFInfo::Geolocation_t const &geo, rapidjson::Value &obj, rapidjson::Document::AllocatorType &alloc) {
    using namespace rapidjson;
    if(!obj.IsObject())
        obj.SetObject();

    // Geolocation

    Value Geolocation(rapidjson::kObjectType);

    Geolocation.AddMember("Latitude",
                          Value(kNumberType).SetDouble(geo.Latitude),
                          alloc);

    Geolocation.AddMember("Longitude",
                          Value(kNumberType).SetDouble(geo.Longitude),
                          alloc);

    Geolocation.AddMember("Altitude",
                          Value(kNumberType).SetDouble(geo.Altitude),
                          alloc);

    Geolocation.AddMember("AltitudeRef",
                          Value(kNumberType).SetDouble(geo.AltitudeRef),
                          alloc);

    Geolocation.AddMember("DOP",
                          Value(kNumberType).SetDouble(geo.DOP),
                          alloc);

    Value LatComponentsObject(rapidjson::kObjectType);
    Value LonComponentsObject(rapidjson::kObjectType);

    get_json(geo.LatComponents, LatComponentsObject, alloc);
    get_json(geo.LonComponents, LonComponentsObject, alloc);

    Geolocation.AddMember("LatComponents", LatComponentsObject.GetObject(),alloc);
    Geolocation.AddMember("LonComponents", LonComponentsObject.GetObject(),alloc);

    obj.AddMember("GeoLocation",
                  Geolocation.GetObject(),
                  alloc);
}

void get_json(easyexif::EXIFInfo::LensInfo_t const &lens, rapidjson::Value &obj, rapidjson::Document::AllocatorType &alloc) {
    using namespace rapidjson;
    if(!obj.IsObject())
        obj.SetObject();

    Value LensInfoObj(rapidjson::kObjectType);

    LensInfoObj.AddMember("FocalLengthMax",
                          Value(kNumberType).SetDouble(lens.FocalLengthMax),
                          alloc);

    LensInfoObj.AddMember("FocalLengthMin",
                          Value(kNumberType).SetDouble(lens.FocalLengthMin),
                          alloc);

    LensInfoObj.AddMember("FStopMax",
                          Value(kNumberType).SetDouble(lens.FStopMax),
                          alloc);

    LensInfoObj.AddMember("FStopMin",
                          Value(kNumberType).SetDouble(lens.FStopMin),
                          alloc);

    LensInfoObj.AddMember("FocalPlaneYResolution",
                          Value(kNumberType).SetDouble(lens.FocalPlaneYResolution),
                          alloc);

    LensInfoObj.AddMember("FocalPlaneXResolution",
                          Value(kNumberType).SetDouble(lens.FocalPlaneXResolution),
                          alloc);

    LensInfoObj.AddMember("Make",
                          Value(kStringType).Set(lens.Make,alloc),
                          alloc);

    LensInfoObj.AddMember("Model",
                          Value(kStringType).Set(lens.Model,alloc),
                          alloc);

    obj.AddMember("LensInfo",
                  LensInfoObj.GetObject(),
                  alloc);

}

int parse_exif_from_file(std::string const file_path, easyexif::EXIFInfo &result) {

    // Read the JPEG file into a buffer
    FILE *fp = fopen(file_path.c_str(), "rb");
    if (!fp) {

        printf("Can't open file : %s for reason: %d \n",file_path.c_str(),errno);
        return PARSE_EXIF_ERROR_NO_EXIF;
    }

    fseek(fp, 0, SEEK_END);
    unsigned long fsize = ftell(fp);
    rewind(fp);
    unsigned char*  buf = new unsigned char[fsize];

    if (fread(buf, 1, fsize, fp) != fsize) {
        printf("Can't read file: %s for reason: %d \n",file_path.c_str(),errno);
        delete[] buf;
        return PARSE_EXIF_ERROR_NO_EXIF;
    }
    fclose(fp);

    auto retval = result.parseFrom(buf,fsize);

    delete[] buf;
    return retval;
}
