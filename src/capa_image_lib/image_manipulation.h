//
// Created by manicqin on 13/12/17.
//

#ifndef CIL_IMAGE_MANIPULATION_H
#define CIL_IMAGE_MANIPULATION_H

#undef BOOST_GIL_EXTENSION_IO_PNG_C_LIB_COMPILED_AS_CPLUSPLUS
#undef BOOST_GIL_EXTENSION_IO_JPEG_C_LIB_COMPILED_AS_CPLUSPLUS

#include <iostream>

#include "utils.h"

#include "boost/gil.hpp"
#include "boost/gil/image.hpp"
#include "boost/gil/typedefs.hpp"
#include "boost/gil/extension/io/jpeg.hpp"
#include "boost/gil/extension/io/png.hpp"
#include "boost/gil/extension/numeric/sampler.hpp"
#include "boost/gil/extension/numeric/resample.hpp"

namespace capa {

    using namespace boost::gil;

    using images_t = boost::mpl::vector<rgb8_image_t, rgba8_image_t>;
    using dynamic_image_t = any_image<images_t>;


    /*
     * This function has a hugh problem, it copies the data from one view to the other.
     * This should not be! This should be fixed!
     * The reason why I did it is because I couldn't find a way to cast the result of rotatexxx flippedxxx
     * over SrcMetaView (either an image or any_image) to DstMetaView
     * there are two solutions
     *
     * 1. stackpverflow github blablablabla
     * 2. make normalize_orientation_view the last function in the pipeline and instead of copying the pixels I can
     * just write the view...
     *
     * I'll add it to the backlog...
     * */
    template<typename SrcMetaView, typename Img>
    void normalize_orientation_view(const SrcMetaView &src, Img &dst, EXIF_ORIENTATION orientation) {

        switch (orientation) {
            case Orientation_8: {
                copy_pixels(rotated90ccw_view(src), view(dst));

                break;
            }
            case Orientation_7: {
                copy_pixels(flipped_left_right_view(rotated90ccw_view(src)), view(dst));
                break;
            }
            case Orientation_6: {
                copy_pixels(rotated90cw_view(src), view(dst));
                break;
            }
            case Orientation_5: {
                copy_pixels(flipped_left_right_view(rotated90cw_view(src)), view(dst));
                break;
            }
            case Orientation_4: {
                copy_pixels(flipped_left_right_view(rotated180_view(src)), view(dst));
                break;
            }
            case Orientation_3: {
                copy_pixels(rotated180_view(src), view(dst));
                break;
            }
            case Orientation_2: {
                copy_pixels(flipped_left_right_view(src), view(dst));
                break;
            }
            case Orientation_1:
            default: {
                copy_pixels(src, view(dst));
                break;
            }
        }
    }

    template<class Tag, class Image>
    bool create_thumbnail_imp(std::istream &in_buffer,
                              std::ostream &out_buffer,
                              unsigned int orientation,
                              unsigned int width, unsigned int height,
                              error_object &out_err) {
        using namespace boost::gil;

        dynamic_image_t dynamic_img = dynamic_image_t(Image());

        try {
            read_image(in_buffer, dynamic_img, Tag());  //This could be easily expend to support multiple formats ...
        }
        catch (const std::ios_base::failure &e) {
            out_err = error_object::create_error(__FUNCTION__,
                                                 {"Failed reading",
                                                  "Caught an ios_base::failure.\n Explanatory string: ",
                                                  e.what()},
                                                 e.code().value());

            return false;
        }
        catch (std::bad_cast const &e) {
            out_err = error_object::create_error(__FUNCTION__,
                                                 {"Failed reading",
                                                  "bad cast",
                                                  e.what()},
                                                 -1);

            return false;
        }
        Image envelope;
        envelope.recreate(width, height);
        resize_view(const_view(dynamic_img), view(envelope), bilinear_sampler());

        Image out_view;
        out_view.recreate(width, height);
        auto orient = static_cast<EXIF_ORIENTATION>(orientation);
        normalize_orientation_view(const_view(envelope), out_view, orient);


        try {
            write_view(out_buffer, view(out_view), Tag());
        }
        catch (const std::ios_base::failure &e) {
            out_err = error_object::create_error(__FUNCTION__,
                                                 {"Failed writing",
                                                  "Caught an ios_base::failure.\n Explanatory string: ",
                                                  e.what()},
                                                 e.code().value());

            return false;
        }
        catch (std::bad_cast const &e) {
            out_err = error_object::create_error(__FUNCTION__,
                                                 {"Failed writing",
                                                  "bad cast",
                                                  e.what()},
                                                 -1);

            return false;
        }
        return true;
    }


    template<class Tag, class Image>
    bool resize_image_imp(std::istream &in_buffer,
                          std::ostream &out_buffer,
                          unsigned int width, unsigned int height,
                          error_object &out_err) {
        using namespace boost::gil;
        using namespace std;

        Image dynamic_img;

        try {
            read_image(in_buffer, dynamic_img, Tag());
        }
        catch (const std::ios_base::failure &e) {
            out_err = error_object::create_error(__FUNCTION__,
                                                 {"Failed reading",
                                                  "Caught an ios_base::failure.\n Explanatory string: ",
                                                  e.what()},
                                                 e.code().value());
            return false;
        }

        Image envelope;
        envelope.recreate(width, height);
        resize_view(const_view(dynamic_img), view(envelope), bilinear_sampler());
        std::ostringstream buffer;

        try {
            write_view(buffer, view(envelope), Tag());
        }
        catch (const std::ios_base::failure &e) {
            out_err = error_object::create_error(__FUNCTION__,
                                                 {"Failed writing",
                                                  "Caught an ios_base::failure.\n Explanatory string: ",
                                                  e.what()},
                                                 e.code().value());

            return false;
        }

        out_buffer << buffer.str();

        return true;
    }
}
#endif //CIL_IMAGE_MANIPULATION_H
