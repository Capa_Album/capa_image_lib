#ifndef CIL_CAPA_IMAGE_LIB_H
#define CIL_CAPA_IMAGE_LIB_H

#define RAPIDJSON_HAS_STDSTRING 1
#define RAPIDJSON_HAS_CXX11_RANGE_FOR 1
#define RAPIDJSON_HAS_CXX11_RVALUE_REFS 1

#include "rapidjson/fwd.h"
#include "rapidjson/writer.h"
#include "rapidjson/ostreamwrapper.h"
#include "rapidjson/rapidjson.h"
#include "rapidjson/document.h"
#include "rapidjson/pointer.h"

#include "exif_wrapper.h"
#include "exif.h"
#include "utils.h"
#include "image_meta_data.h"

#include <functional>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

namespace capa {


    using namespace easyexif;
//CIL_EXIF_WRAPPER_H

// For now this function will hard code the supported file types
// Later this function will either be "compile dynamic" using the CMAKE
// or "runtime dynamic" using boost::dll


    bool create_thumbnail_jpg_IFOF(std::string const &in_filename,
                                   std::string const &out_filename,
                                   unsigned int orientation,
                                   unsigned int width, unsigned int height,
                                   error_object &out_err);

    bool create_thumbnail_png_IFOF(std::string const &in_filename,
                                   std::string const &out_filename,
                                   unsigned int orientation,
                                   unsigned int width, unsigned int height,
                                   error_object &out_err);

    bool create_thumbnail_IFOF(std::string const &in_filename,
                               std::string const &out_filename,
                               unsigned int orientation,
                               unsigned int width, unsigned int height,
                               error_object &out_err);

    bool create_thumbnail_jpg_IFOB(std::string const &in_filename,
                                   std::ostream &out_buffer,
                                   unsigned int orientation,
                                   unsigned int width, unsigned int height,
                                   error_object &out_err);

    bool create_thumbnail_png_IFOB(std::string const &in_filename,
                                   std::ostream &out_buffer,
                                   unsigned int orientation,
                                   unsigned int width, unsigned int height,
                                   error_object &out_err);

    bool create_thumbnail_IFOB(std::string const &in_filename,
                               std::ostream &out_buffer,
                               unsigned int orientation,
                               unsigned int width, unsigned int height,
                               error_object &out_err);


    bool create_thumbnail_jpg_IBOB(std::istream &in_buffer,
                                   std::ostream &out_buffer,
                                   unsigned int orientation,
                                   unsigned int width, unsigned int height,
                                   error_object &out_err);

    bool create_thumbnail_png_IBOB(std::istream &in_buffer,
                                   std::ostream &out_buffer,
                                   unsigned int orientation,
                                   unsigned int width, unsigned int height,
                                   error_object &out_err);

    bool create_thumbnail_IBOB(std::istream &in_buffer,
                               std::ostream &out_buffer,
                               unsigned int orientation,
                               unsigned int width, unsigned int height,
                               error_object &out_err);


    bool resize_image_jpg_IFOF(std::string const &in_filename,
                               std::string const &out_filename,
                               unsigned int width, unsigned int height,
                               error_object &out_err);

    bool resize_image_png_IFOF(std::string const &in_filename,
                               std::string const &out_filename,
                               unsigned int width, unsigned int height,
                               error_object &out_err);

    bool resize_image_IFOF(std::string const &in_filename,
                           std::string const &out_filename,
                           unsigned int width, unsigned int height,
                           error_object &out_err);

    bool resize_image_png_IFOB(std::string const &in_filename,
                               std::ostream &out_buffer,
                               unsigned int width, unsigned int height,
                               error_object &out_err);

    bool resize_image_jpg_IFOB(std::string const &in_filename,
                               std::ostream &out_buffer,
                               unsigned int width, unsigned int height,
                               error_object &out_err);

    bool resize_image_IFOB(std::string const &in_filename,
                           std::ostream &out_buffer,
                           unsigned int width, unsigned int height,
                           error_object &out_err);

    bool resize_image_IBOB(std::istream &in_buffer,
                           std::ostream &out_buffer,
                           unsigned int width, unsigned int height,
                           error_object &out_err);

    int read_picture_IF(std::string_view file_path, easyexif::EXIFInfo &result);

    void extract_exif_jpg_IF(std::string_view file_name, rapidjson::Value &doc, rapidjson::Document::AllocatorType &alloc);

    void extract_exif_jpg_IB(std::istream &buf, rapidjson::Value &doc, rapidjson::Document::AllocatorType &alloc);

    void extract_exif_jpg_IF(rapidjson::Value const &file_names, rapidjson::Value &doc,
                             rapidjson::Document::AllocatorType &alloc);

    void extract_exif_jpg_IF(std::vector<std::string> const &file_names, rapidjson::Value &doc,
                             rapidjson::Document::AllocatorType &alloc);

    bool extract_picture_info(std::string_view in_filename, rapidjson::Value &doc,
                              rapidjson::Document::AllocatorType &alloc);

    void extract_picture_info(rapidjson::Value const &file_names, rapidjson::Value &doc,
                              rapidjson::Document::AllocatorType &alloc);

    void extract_picture_info(std::vector<std::string> const &file_names, rapidjson::Value &doc,
                              rapidjson::Document::AllocatorType &alloc);
}
#endif
