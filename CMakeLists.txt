cmake_minimum_required(VERSION 3.0)
add_definitions(-std=gnu++17)

option(BUILD_CIL_INSTRUMENTAL "Build the CIL Test suite" OFF)

message(STATUS "Build BUILD_CIL_INSTRUMENTAL ${BUILD_CIL_INSTRUMENTAL}")

#set(USER_LJPEG_LIBRARY "/home/manicqin/Projects/libjpeg-turbo/libjpeg.so")
#set(USER_LJPEG_INCLUDE_DIR "/home/manicqin/Projects/libjpeg-turbo/")

if(CMAKE_BUILD_TYPE MATCHES Debug)
    set (CMAKE_CXX_FLAGS "-DBOOST_GIL_USE_CONCEPT_CHECK -Wno-deprecated-declarations")
    if (${BUILD_CIL_INSTRUMENTAL})
        set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -fprofile-arcs -ftest-coverage -O0")
        set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -fprofile-arcs -ftest-coverage -O0")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fprofile-arcs -ftest-coverage -O0")
        set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fprofile-arcs -ftest-coverage -O0")
        message(STATUS "Build Instrumental")
    endif ()
else()
    set (CMAKE_CXX_FLAGS "-O2 -DNDEBUG -DBOOST_GIL_USE_CONCEPT_CHECK -Wno-deprecated-declarations")
endif()

project (cil)

message(STATUS "start CMakeLists.txt NODE_ARCH:${NODE_ARCH} CMAKE_JS_INC:${CMAKE_JS_INC}")
include(${CMAKE_CURRENT_SOURCE_DIR}/src/CMakeLists.txt)
message(STATUS "end CMakeLists.txt")

###################
#
#
#   Building profiles
#       Defaults when building with CMake
#
#           BUILD_CIL_LIB=      ON
#           BUILD_CIL_MODULE=   OFF
#           BUILD_CIL_TESTER=   OFF
#
#       Release
#       Debug
#
#       Debugging Node
#
#           Needs to supply:
#               -DNODE_RUNTIME="node"/"electron"
#               -DNODE_RUNTIMEVERSION="6.11.4"
#               -DNODE_ARCH=  example: "x64"
#               -DCMAKE_JS_VERSION= example: "3.4.1"
#               -DCMAKE_JS_INC=example: ~/.cmake-js/node-x64/v6.11.4/include/node;
#
#           Read more:
#               https://github.com/cmake-js/cmake-js/wiki/TUTORIAL-02-Creating-CMake.js-based-native-addons-with-QT-Creator
#       Defaults when building with cmake-js
#           BUILD_CIL_LIB=      ON
#           BUILD_CIL_MODULE=   ON
#           BUILD_CIL_TESTER=   OFF
#
#           CMAKE_JS_INC=       will be supplied by cmake-js
#       Release
#           BUILD_CIL_LIB=      ON
#       Debug
#
#       Notes:
#       *   Weird problem with Clion 2017.3, you can create profiles in settings -> Build,Ex.... -> CMake
#           But only the top most profile will set the environment variables ...
#       *   Remeber to clean your cache :(


#-DCMAKE_JS_VERSION="3.4.1" -DCMAKE_BUILD_TYPE="Release" -DCMAKE_LIBRARY_OUTPUT_DIRECTORY="/home/manicqin/Projects/capa_image_lib/build/Release" -DCMAKE_JS_INC="/home/manicqin/.cmake-js/node-x64/v6.11.4/include/node;/home/manicqin/Projects/capa_image_lib/node_modules/nan" -DNODE_RUNTIME="node" -DNODE_RUNTIMEVERSION="6.11.4" -DNODE_ARCH="x64" -DCMAKE_CXX_FLAGS="-std=c++11"
