//
// Created by manicqin on 31/10/17.
//
#include "utils.h"

#include <rapidjson/writer.h>
#include <rapidjson/ostreamwrapper.h>
#include <rapidjson/document.h>
#include <rapidjson/pointer.h>

namespace capa {
    rapidjson::Value & get_error_json(error_object const& in_err, rapidjson::Value &obj, rapidjson::Document::AllocatorType & alloc) {

        if(!obj.IsObject())
        {
            obj.SetObject();
        }

        obj.AddMember("function_name",in_err.function_name, alloc);
        obj.AddMember("err_description",in_err.err_description, alloc);
        obj.AddMember("err_code",in_err.err_code, alloc);
        obj.AddMember("failed",in_err.fail(), alloc);

        return obj;
    }

    //////////////////////////////////////////////////////EXIF///////////////////////////////////////////////////////

    std::string get_json_text(rapidjson::Value const &in_doc) {
        rapidjson::StringBuffer buffer;

        buffer.Clear();

        rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
        in_doc.Accept(writer);

        return std::string(buffer.GetString());
    }
}