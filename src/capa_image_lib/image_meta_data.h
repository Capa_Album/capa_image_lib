//
// Created by manicqin on 13/12/17.
//

#ifndef CIL_IMAGE_META_DATA_H
#define CIL_IMAGE_META_DATA_H

#include <string_view>

namespace capa {

    enum FILE_TYPES {
        Undefined = 0,
        Jpeg,
        Png,
        Bmp,
        Tiff,
        Raw
    };

    FILE_TYPES file_type_by_filename(std::string_view file_name);

    FILE_TYPES file_type_by_stream(std::istream &buffer);

    template<typename T>
    FILE_TYPES file_type_by_template(T);


    enum EXIF_ORIENTATION {
        Orientation_1 = 1,          //Nothing
        Orientation_2,              //Flip left right
        Orientation_3,              //Rotate 180
        Orientation_4,              //Flip up down
        Orientation_5,              //Rotate 90 + Flip lr
        Orientation_6,              //Rotate 90
        Orientation_7,              //Rotate -90 + Flip lr
        Orientation_8               //Rotate -90
    };

}
#endif //CIL_IMAGE_META_DATA_H
