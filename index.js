/**
 * Created by manicqin on 12/05/17.
 */

const cil = require("bindings")("cil_node");

const is_string = (str) => {
    return typeof str === 'string' || str instanceof String;
};

const jpg = {
    create_thumbnail_IFOF_async: function (original_file_name, resized_file_name, orientation, w, h) {
        return new Promise((resolve, reject) => {
            cil.create_thumbnail_jpg_IFOF_async(original_file_name,
                resized_file_name,
                orientation,
                w,
                h,
                (error, result) => {
                    if (error) {
                        return reject(error);
                    } else {
                        return resolve(result);
                    }
                });
        });
    },
    create_thumbnail_IFOB_async: function (original_file_name, orientation, w, h) {
        return new Promise((resolve, reject) => {
            cil.create_thumbnail_jpg_IFOB_async(original_file_name,
                orientation,
                w,
                h,
                (error, result) => {
                    if (error) {
                        return reject(error);
                    } else {
                        return resolve(result);
                    }
                });
        });
    },
    create_thumbnail_IBOB_async: function (original_buffer, orientation, w, h) {
        return new Promise((resolve, reject) => {
            cil.create_thumbnail_jpg_IBOB_async(original_buffer,
                orientation,
                w,
                h,
                (error, result) => {
                    if (error) {
                        return reject(error);
                    } else {
                        return resolve(result);
                    }
                });
        });
    },

    create_thumbnail_IFOF: function (original_file_name, resized_file_name, orientation, w, h) {
        cil.create_thumbnail_jpg_IFOF(original_file_name,
            resized_file_name,
            orientation,
            w,
            h);
    },
    create_thumbnail_IFOB: function (original_file_name, orientation, w, h) {
        return cil.create_thumbnail_jpg_IFOB(original_file_name,
            orientation,
            w,
            h);
    },
    create_thumbnail_IBOB: function (original_buffer, orientation, w, h) {
        return cil.create_thumbnail_jpg_IBOB(original_buffer,
            orientation,
            w,
            h);
    },

    extract_exif_jpg_IF: function (original_file_name) {
        var retval = cil.extract_exif_jpg_IF(original_file_name);
        return JSON.parse(retval);
    },
    extract_exif_jpg_IB: function (file_buffer) {
        var retval = cil.extract_exif_jpg_IB(file_buffer);
        return JSON.parse(retval);
    }
};
const png = {
    create_thumbnail_IFOF_async: function (original_file_name, resized_file_name, orientation, w, h) {
        return new Promise((resolve, reject) => {
            cil.create_thumbnail_png_IFOF_async(original_file_name,
                resized_file_name,
                orientation,
                w,
                h,
                (error, result) => {
                    if (error) {
                        return reject(error);
                    } else {
                        return resolve(result);
                    }
                });
        });
    },
    create_thumbnail_IFOB_async: function (original_file_name, orientation, w, h) {
        return new Promise((resolve, reject) => {
            cil.create_thumbnail_png_IFOB_async(original_file_name,
                orientation,
                w,
                h,
                (error, result) => {
                    if (error) {
                        return reject(error);
                    } else {
                        return resolve(result);
                    }
                });
        });
    },
    create_thumbnail_IBOB_async: function (original_buffer, orientation, w, h) {
        return new Promise((resolve, reject) => {
            cil.create_thumbnail_png_IBOB_async(original_buffer,
                orientation,
                w,
                h,
                (error, result) => {
                    if (error) {
                        return reject(error);
                    } else {
                        return resolve(result);
                    }
                });
        });
    },

    create_thumbnail_IFOF: function (original_file_name, resized_file_name, orientation, w, h) {
        cil.create_thumbnail_png_IFOF(original_file_name,
            resized_file_name,
        orientation,
        w,
            h);
    },
    create_thumbnail_IFOB: function (original_file_name, orientation, w, h) {
    return cil.create_thumbnail_png_IFOB(original_file_name,
        orientation,
        w,
        h);
    },
    create_thumbnail_IBOB: function (original_buffer, orientation, w, h) {
        return cil.create_thumbnail_png_IBOB(original_buffer,
        orientation,
        w,
        h)
}
};
const generic = {
    create_thumbnail_IFOF_async: function (original_file_name, resized_file_name, orientation, w, h) {
        return new Promise((resolve, reject) => {
            cil.create_thumbnail_IFOF_async(original_file_name,
                resized_file_name,
                orientation,
                w,
                h,
                (error, result) => {
                    if (error) {
                        return reject(error);
                    } else {
                        return resolve(result);
                    }
                });
        });
    },
    create_thumbnail_IFOB_async: function (original_file_name, orientation, w, h) {
        return new Promise((resolve, reject) => {
            cil.create_thumbnail_IFOB_async(original_file_name,
                orientation,
                w,
                h,
                (error, result) => {
                    if (error) {
                        return reject(error);
                    } else {
                        return resolve(result);
                    }
                });
        });
    },
    create_thumbnail_IBOB_async: function (original_buffer, orientation, w, h) {
    return new Promise((resolve, reject) => {
        cil.create_thumbnail_IBOB_async(original_buffer,
            orientation,
            w,
            h,
            (error, result) => {
                if (error) {
                    return reject(error);
                } else {
                    return resolve(result);
                }
            });
    });
    },

    create_thumbnail_IFOF: function (original_file_name, resized_file_name, orientation, w, h) {
        cil.create_thumbnail_IFOF(original_file_name,
            resized_file_name,
            orientation,
            w,
            h);
    },
    create_thumbnail_IFOB: function (original_file_name, orientation, w, h) {
        return cil.create_thumbnail_IFOB(original_file_name,
            orientation,
            w,
            h);
    },
    create_thumbnail_IBOB: function (original_buffer, orientation, w, h) {
        return cil.create_thumbnail_IBOB(original_buffer,
            orientation,
            w,
            h);
    },

    resize_image_IFOF: function (original_file_name, out_file_name, w, h) {
        return cil.resize_image_IFOF(original_file_name, out_file_name, w, h);
    },
    resize_image_IFOB: function (original_file_name, w, h) {
        return cil.resize_image_IFOB(original_file_name, w, h);
    },
    resize_image_IBOB: function (original_buffer, w, h) {
        return cil.resize_image_IBOB(original_buffer, w, h);
    }
};


module.exports = {
    create_thumbnail_async: function (input, options) {

        const promise = (error, result) => {
            if (error) {
                return reject(error);
            } else {
                return resolve(result);
            }
        };

        return new Promise((resolve, reject) => {

            if (is_string(input)) {
                if (is_string(options.output)) {
                    return cil.create_thumbnail_IFOF_async(input,
                        options.output,
                        options.orientation,
                        options.width,
                        options.height,
                        promise);
                } else {
                    return cil.create_thumbnail_IFOB_async(input,
                        options.orientation,
                        options.width,
                        options.height,
                        promise);
                }

            } else {
                cil.create_thumbnail_IBOB_async(input,
                    options.orientation,
                    options.width,
                    options.height,
                    promise);
            }
        });
    },
    create_thumbnail: function (input, options) {

        // let options = {
        //     width,
        //     height,
        //      output,
        //      orientation
        // };

        if (is_string(input)) {
            if (is_string(options.output)) {
                return cil.create_thumbnail_IFOF(input, options.output, options.orientation, options.width, options.height);
            } else {
                return cil.create_thumbnail_IFOB(input, options.orientation, options.width, options.height);
            }

        } else {
            return cil.create_thumbnail_IBOB(input, options.orientation, options.width, options.height);
        }

        return null;
    },

    resize_image: function (input, options) {

        if (is_string(input)) {
            if (is_string(options.output)) {
                return cil.resize_image_IFOF(input, options.output, options.width, options.height);
            } else {
                return cil.resize_image_IFOB(input, options.width, options.height);
            }

        } else {
            return cil.resize_image_IBOB(input, options.width, options.height);
        }

        return null;
    },

    get_exif: function (file_array) {

        var str_file_array = "";

        if ((typeof file_array) !== "string") {
            str_file_array = JSON.stringify(file_array);
        } else {
            str_file_array = file_array;
        }

        var retval = cil.get_exif(str_file_array);

        return JSON.parse(retval);
    },
    get_picture_info: function (file_array) {
        var str_file_array = "";

        if ((typeof file_array) !== "string") {
            str_file_array = JSON.stringify(file_array);
        } else {
            str_file_array = file_array;
        }

        var retval = cil.get_picture_info(str_file_array);
        return JSON.parse(retval);

    },

    jpg,
    png,

    generic
};


