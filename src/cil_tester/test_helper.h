//
// Created by manicqin on 15/10/17.
//

#ifndef CIL_TEST_HELPER_H
#define CIL_TEST_HELPER_H

#include <string>
#include <vector>

bool test_create_thumbnail_from_file_jpg(std::string const &in_file_name, std::string const &out_file_name);

bool test_create_thumbnail_from_file_png(std::string const &in_file_name, std::string const &out_file_name);

bool test_create_thumbnail_from_buffer_jpg(std::string const &in_file_name);

bool test_create_thumbnail_from_buffer_png(std::string const &in_file_name);

bool test_resize(std::string const &in_file_name, std::string const &out_file_name);

bool test_exif_from_buffer(std::string const &in_file_name);

bool test_get_info(std::string const &in_file_name);

bool test_get_info_array(std::vector<std::string> const &in_arr_file_name);

#endif //CIL_TEST_HELPER_H
