#include "capa_image_lib.h"

#include <node.h>
#include <nan.h>

namespace cil{
    using v8::Exception;
    using v8::FunctionCallbackInfo;
    using v8::Isolate;
    using v8::Local;
    using v8::Number;
    using v8::Object;
    using v8::String;
    using v8::Value;
    using v8::Function;

    template<bool F(std::istream &,
                    std::ostream &,
                    unsigned int,
                    unsigned int, unsigned int,
                    capa::error_object &)>
    class Worker_create_thumbnail_IBOB : public Nan::AsyncWorker {
      public:
        Worker_create_thumbnail_IBOB(std::istream &in_buffer,
                                     unsigned int orientation,
                                     unsigned int width,
                                     unsigned int height,
                                     Nan::Callback *callback)
            : m_out_buffer(std::ios_base::out | std::ios_base::binary),
              m_orientation(orientation),
              m_width(width),
              m_height(height),
              Nan::AsyncWorker(callback) {

            m_in_buffer << in_buffer.rdbuf();

        }

        void Execute() override {
            capa::error_object out_err;

            if (!F(m_in_buffer, m_out_buffer, m_orientation, m_width, m_height, out_err)) {

                rapidjson::Document dom;
                auto retval = capa::get_json_text(capa::get_error_json(out_err, dom, dom.GetAllocator()));
                SetErrorMessage(retval.c_str());
            }
        }

        void HandleOKCallback() override {
            Nan::HandleScope scope;

            auto str_retval = m_out_buffer.str();
            auto retval = Nan::CopyBuffer(str_retval.c_str(), str_retval.size());


            v8::Local<v8::Value> argv[] = {
                Nan::Null(), // no error occured
                retval.ToLocalChecked()
            };
            callback->Call(2, argv);
        }

        void HandleErrorCallback() override {
            Nan::HandleScope scope;
            v8::Local<v8::Value> argv[] = {
                Nan::New(this->ErrorMessage()).ToLocalChecked(), // return error message
                Nan::Null()
            };
            callback->Call(2, argv);
        }

      private:
        std::stringstream m_in_buffer;
        std::ostringstream m_out_buffer;
        unsigned int m_orientation;
        unsigned int m_width;
        unsigned int m_height;
    };

    template<bool F(std::string const &,
                    std::ostream &,
                    unsigned int,
                    unsigned int,
                    unsigned int,
                    capa::error_object &)>
    class Worker_create_thumbnail_IFOB : public Nan::AsyncWorker {
      public:
        Worker_create_thumbnail_IFOB(std::string const &in_filename,
                                     unsigned int orientation,
                                     unsigned int width,
                                     unsigned int height,
                                     Nan::Callback *callback)
            : m_filename(in_filename),
              m_out_buffer(std::ios_base::out | std::ios_base::binary),
              m_orientation(orientation),
              m_width(width),
              m_height(height),
              Nan::AsyncWorker(callback) {}

        void Execute() override {
            capa::error_object out_err;

            if (!F(m_filename, m_out_buffer, m_orientation, m_width, m_height, out_err)) {

                rapidjson::Document dom;
                auto retval = capa::get_json_text(capa::get_error_json(out_err, dom, dom.GetAllocator()));
                SetErrorMessage(retval.c_str());
            }
        }

        void HandleOKCallback() override {
            Nan::HandleScope scope;

            auto str_retval = m_out_buffer.str();
            auto retval = Nan::CopyBuffer(str_retval.c_str(), str_retval.size());


            v8::Local<v8::Value> argv[] = {
                Nan::Null(), // no error occured
                retval.ToLocalChecked()
            };
            callback->Call(2, argv);
        }

        void HandleErrorCallback() override {
            Nan::HandleScope scope;
            v8::Local<v8::Value> argv[] = {
                Nan::New(this->ErrorMessage()).ToLocalChecked(), // return error message
                Nan::Null()
            };
            callback->Call(2, argv);
        }

      private:
        std::string m_filename;
        std::ostringstream m_out_buffer;
        unsigned int m_orientation;
        unsigned int m_width;
        unsigned int m_height;
    };

    template<bool F(std::string const &,
                    std::string const &,
                    unsigned int,
                    unsigned int,
                    unsigned int,
                    capa::error_object &)>
    class Worker_create_thumbnail_IFOF : public Nan::AsyncWorker {
      public:
        Worker_create_thumbnail_IFOF(std::string const &in_filename,
                                     std::string &out_filename,
                                     unsigned int orientation,
                                     unsigned int width,
                                     unsigned int height,
                                     Nan::Callback *callback)
            : m_in_filename(in_filename),
              m_out_filename(out_filename),
              m_orientation(orientation),
              m_width(width),
              m_height(height),
              Nan::AsyncWorker(callback) {}

        void Execute() override {
            capa::error_object out_err;

            if (!F(m_in_filename, m_out_filename, m_orientation, m_width, m_height, out_err)) {

                rapidjson::Document dom;
                auto retval = capa::get_json_text(capa::get_error_json(out_err, dom, dom.GetAllocator()));
                SetErrorMessage(retval.c_str());
            }
        }

        void HandleOKCallback() override {
            Nan::HandleScope scope;

            auto retval = Nan::CopyBuffer(m_in_filename.c_str(), m_in_filename.size());


            v8::Local<v8::Value> argv[] = {
                Nan::Null(), // no error occured
                retval.ToLocalChecked()
            };
            callback->Call(2, argv);
        }

        void HandleErrorCallback() override {
            Nan::HandleScope scope;
            v8::Local<v8::Value> argv[] = {
                Nan::New(this->ErrorMessage()).ToLocalChecked(), // return error message
                Nan::Null()
            };
            callback->Call(2, argv);
        }

      private:
        std::string m_in_filename;
        std::string m_out_filename;
        unsigned int m_orientation;
        unsigned int m_width;
        unsigned int m_height;
    };


    template<bool F(std::istream &,
                    std::ostream &,
                    unsigned int, unsigned int,
                    capa::error_object &)>
    class Worker_resize_image_IBOB : public Nan::AsyncWorker {
      public:
        Worker_resize_image_IBOB(std::istream &in_buffer,
                                 unsigned int width,
                                 unsigned int height,
                                 Nan::Callback *callback)
            : m_out_buffer(std::ios_base::out | std::ios_base::binary),
              m_width(width),
              m_height(height),
              Nan::AsyncWorker(callback) {

            m_in_buffer << in_buffer.rdbuf();

        }

        void Execute() override {
            capa::error_object out_err;

            if (!F(m_in_buffer, m_out_buffer, m_width, m_height, out_err)) {

                rapidjson::Document dom;
                auto retval = capa::get_json_text(capa::get_error_json(out_err, dom, dom.GetAllocator()));
                SetErrorMessage(retval.c_str());
            }
        }

        void HandleOKCallback() override {
            Nan::HandleScope scope;

            auto str_retval = m_out_buffer.str();
            auto retval = Nan::CopyBuffer(str_retval.c_str(), str_retval.size());


            v8::Local<v8::Value> argv[] = {
                Nan::Null(), // no error occured
                retval.ToLocalChecked()
            };
            callback->Call(2, argv);
        }

        void HandleErrorCallback() override {
            Nan::HandleScope scope;
            v8::Local<v8::Value> argv[] = {
                Nan::New(this->ErrorMessage()).ToLocalChecked(), // return error message
                Nan::Null()
            };
            callback->Call(2, argv);
        }

      private:
        std::stringstream m_in_buffer;
        std::ostringstream m_out_buffer;
        unsigned int m_width;
        unsigned int m_height;
    };

    template<bool F(std::string const &,
                    std::ostream &,
                    unsigned int,
                    unsigned int,
                    capa::error_object &)>
    class Worker_resize_image_IFOB : public Nan::AsyncWorker {
      public:
        Worker_resize_image_IFOB(std::string const &in_filename,
                                 unsigned int width,
                                 unsigned int height,
                                 Nan::Callback *callback)
            : m_filename(in_filename),
              m_out_buffer(std::ios_base::out | std::ios_base::binary),
              m_width(width),
              m_height(height),
              Nan::AsyncWorker(callback) {}

        void Execute() override {
            capa::error_object out_err;

            if (!F(m_filename, m_out_buffer, m_width, m_height, out_err)) {

                rapidjson::Document dom;
                auto retval = capa::get_json_text(capa::get_error_json(out_err, dom, dom.GetAllocator()));
                SetErrorMessage(retval.c_str());
            }
        }

        void HandleOKCallback() override {
            Nan::HandleScope scope;

            auto str_retval = m_out_buffer.str();
            auto retval = Nan::CopyBuffer(str_retval.c_str(), str_retval.size());


            v8::Local<v8::Value> argv[] = {
                Nan::Null(), // no error occured
                retval.ToLocalChecked()
            };
            callback->Call(2, argv);
        }

        void HandleErrorCallback() override {
            Nan::HandleScope scope;
            v8::Local<v8::Value> argv[] = {
                Nan::New(this->ErrorMessage()).ToLocalChecked(), // return error message
                Nan::Null()
            };
            callback->Call(2, argv);
        }

      private:
        std::string m_filename;
        std::ostringstream m_out_buffer;
        unsigned int m_width;
        unsigned int m_height;
    };

    template<bool F(std::string const &,
                    std::string const &,
                    unsigned int,
                    unsigned int,
                    capa::error_object &)>
    class Worker_resize_image_IFOF : public Nan::AsyncWorker {
      public:
        Worker_resize_image_IFOF(std::string const &in_filename,
                                 std::string &out_filename,
                                 unsigned int width,
                                 unsigned int height,
                                 Nan::Callback *callback)
            : m_in_filename(in_filename),
              m_out_filename(out_filename),
              m_width(width),
              m_height(height),
              Nan::AsyncWorker(callback) {}

        void Execute() override {
            capa::error_object out_err;

            if (!F(m_in_filename, m_out_filename, m_width, m_height, out_err)) {

                rapidjson::Document dom;
                auto retval = capa::get_json_text(capa::get_error_json(out_err, dom, dom.GetAllocator()));
                SetErrorMessage(retval.c_str());
            }
        }

        void HandleOKCallback() override {
            Nan::HandleScope scope;

            auto retval = Nan::CopyBuffer(m_in_filename.c_str(), m_in_filename.size());


            v8::Local<v8::Value> argv[] = {
                Nan::Null(), // no error occured
                retval.ToLocalChecked()
            };
            callback->Call(2, argv);
        }

        void HandleErrorCallback() override {
            Nan::HandleScope scope;
            v8::Local<v8::Value> argv[] = {
                Nan::New(this->ErrorMessage()).ToLocalChecked(), // return error message
                Nan::Null()
            };
            callback->Call(2, argv);
        }

      private:
        std::string m_in_filename;
        std::string m_out_filename;
        unsigned int m_width;
        unsigned int m_height;
    };


    //get_picture_info_jpg_IAF
    void get_picture_info(const FunctionCallbackInfo<Value>& info) {
        Isolate* isolate = info.GetIsolate();

        rapidjson::Document docFiles;
        rapidjson::Document docPoints;

        //TODO: There must be a better way to do the conversion to V8::String
        if (info.Length() >= 1)
        {
            // Check the argument types
            if (!info[0]->IsString()) {
                isolate->ThrowException(Exception::TypeError(
                        String::NewFromUtf8(isolate, "Param 1 - not a string")));
                return;
            }

            v8::String::Utf8Value param1(info[0]->ToString());

            std::stringstream  json_stream;
            json_stream << std::string(*param1);

            docFiles.Parse(std::string(json_stream.str()).c_str());
            if(docFiles.HasParseError())
            {
                json_stream.clear();

                json_stream << "Param 1 - Parse Error: " << docFiles.GetParseError() << " On "
                            << docFiles.GetErrorOffset();
                Nan::ThrowError(Nan::New(json_stream.str()).ToLocalChecked());

                return;
            }

            capa::extract_picture_info(docFiles,docPoints,docPoints.GetAllocator());
        }


        info.GetReturnValue().Set(String::NewFromUtf8(isolate, capa::get_json_text(docPoints).c_str()));
    }

    //extract_exif_jpg_IAF
    void get_exif(const FunctionCallbackInfo<Value>& info) {
        Isolate* isolate = info.GetIsolate();

        rapidjson::Document docFiles;
        rapidjson::Document docPoints;
        rapidjson::Document docMaps;

        //TODO: There must be a better way to do the conversion to V8::String
        if (info.Length() >= 1)
        {
            // Check the argument types
            if (!info[0]->IsString()) {
                isolate->ThrowException(Exception::TypeError(
                        String::NewFromUtf8(isolate, "Param 1 - not a string")));
                return;
            }

            v8::String::Utf8Value param1(info[0]->ToString());

            std::stringstream  json_stream;
            json_stream << std::string(*param1);

            docFiles.Parse(std::string(json_stream.str()).c_str());
            if(docFiles.HasParseError())
            {
                json_stream.clear();

                json_stream << "Param 1 - Parse Error: " << docFiles.GetParseError() << " On "
                            << docFiles.GetErrorOffset();

                isolate->ThrowException(Exception::TypeError(
                        String::NewFromUtf8(isolate,std::string(json_stream.str()).c_str() )));

                return;
            }

            capa::extract_exif_jpg_IF(docFiles,docPoints,docPoints.GetAllocator());
        }

        info.GetReturnValue().Set(String::NewFromUtf8(isolate, capa::get_json_text(docPoints).c_str()));
    }

    void extract_exif_jpg_IF(const FunctionCallbackInfo<Value>& info) {
        Isolate* isolate = info.GetIsolate();

        v8::String::Utf8Value param1(info[0]->ToString());

        std::stringstream  filename_stream;
        filename_stream << std::string(*param1);
        rapidjson::Document docPoints;

        Local<String> retval = String::NewFromUtf8(isolate,std::string(filename_stream.str()).c_str());

        capa::extract_exif_jpg_IF(std::string(filename_stream.str()).c_str(),docPoints,docPoints.GetAllocator());

        info.GetReturnValue().Set(String::NewFromUtf8(isolate, capa::get_json_text(docPoints).c_str()));
    }

    void extract_exif_jpg_IB(const FunctionCallbackInfo<Value>& info) {
        Isolate* isolate = info.GetIsolate();

        Nan::TypedArrayContents<char> data(info[0]);

        std::stringstream  in_buffer;
        in_buffer.write(*data, data.length());
        rapidjson::Document docPoints;

        capa::extract_exif_jpg_IB(in_buffer,docPoints,docPoints.GetAllocator());

        info.GetReturnValue().Set(String::NewFromUtf8(isolate, capa::get_json_text(docPoints).c_str()));
    }


    void resize_image_IBOB(const FunctionCallbackInfo<Value> &info) {


        Nan::TypedArrayContents<char> data(info[0]);

        std::stringstream in_buffer;
        in_buffer.write(*data, data.length());

        v8::String::Utf8Value param1(info[1]->ToString());

        auto width = Nan::To<u_int32_t>(info[2]).FromJust();
        auto height = Nan::To<u_int32_t>(info[3]).FromJust();

        std::stringstream in_memory(std::ios_base::in | std::ios_base::out | std::ios_base::binary);

        capa::error_object err;
        capa::resize_image_IBOB(in_buffer, in_memory, width, height, err);

        auto str_retval = in_memory.str();
        auto retval = Nan::CopyBuffer(str_retval.c_str(), str_retval.size());

        if (!retval.IsEmpty()) {
            info.GetReturnValue().Set(retval.ToLocalChecked());
        } else {
            info.GetReturnValue().Set(false);
        }

    }

    void resize_image_IFOB(const FunctionCallbackInfo<Value>& info) {

        //TODO: TEST MOTHER FUCKER!
        v8::String::Utf8Value param1(info[0]->ToString());

        auto width = Nan::To<u_int32_t>(info[1]).FromJust();
        auto height = Nan::To<u_int32_t>(info[2]).FromJust();

        std::string  in_filename = std::string(*param1);
        std::stringstream in_memory( std::ios_base::in | std::ios_base::out | std::ios_base::binary );

        capa::error_object  err;
        capa::resize_image_IFOB(in_filename, in_memory, width, height, err);

        auto str_retval = in_memory.str();
        auto retval = Nan::CopyBuffer(str_retval.c_str(),str_retval.size());

        if(!retval.IsEmpty())
        {
            info.GetReturnValue().Set(retval.ToLocalChecked());
        }
        else{
            info.GetReturnValue().Set(false);
        }

    }

    void resize_image_IFOF(const FunctionCallbackInfo<Value>& info) {
        Isolate* isolate = info.GetIsolate();

        //TODO: TEST MOTHER FUCKER!
        v8::String::Utf8Value param1(info[0]->ToString());
        v8::String::Utf8Value param2(info[1]->ToString());

        auto width = Nan::To<u_int32_t>(info[2]).FromJust();
        auto height = Nan::To<u_int32_t>(info[3]).FromJust();

        std::string  in_filename = std::string(*param1);
        std::string  out_filename = std::string(*param2);

        std::string retval = "";
        capa::error_object  err;

        retval = capa::resize_image_IFOF(in_filename, out_filename, width, height, err);

        info.GetReturnValue().Set(String::NewFromUtf8(isolate,retval.c_str()));
    }

    template<bool F(std::istream &,
                    std::ostream &,
                    unsigned int, unsigned int,
                    capa::error_object &)>
    void resize_image_IBOB_async(const FunctionCallbackInfo<Value> &info) {
        Nan::TypedArrayContents<char> data(info[0]);

        auto width = Nan::To<u_int32_t>(info[1]).FromJust();
        auto height = Nan::To<u_int32_t>(info[2]).FromJust();

        auto callback = new Nan::Callback(info[3].As<v8::Function>());

        auto data_length = data.length();
        std::stringstream in_buffer;
        in_buffer.write(*data, data_length);

        capa::error_object err;

        if (in_buffer.good()) {
            AsyncQueueWorker(new Worker_resize_image_IBOB<F>(in_buffer,
                                                             width,
                                                             height,
                                                             callback));
        } else {
            err = capa::error_object::create_error(__FUNCTION__,
                                                   "Stream error: ",
                                                   in_buffer.rdstate());
            Isolate *isolate = info.GetIsolate();

            rapidjson::Document dom;
            auto retval = capa::get_json_text(capa::get_error_json(err, dom, dom.GetAllocator()));
            info.GetReturnValue().Set(String::NewFromUtf8(isolate, retval.c_str()));
        }
    }

    template<bool F(std::string const &,
                    std::ostream &,
                    unsigned int, unsigned int,
                    capa::error_object &)>
    void resize_image_IFOB_async(const FunctionCallbackInfo<Value> &info) {
        v8::String::Utf8Value param1(info[0]->ToString());

        auto width = Nan::To<u_int32_t>(info[1]).FromJust();
        auto height = Nan::To<u_int32_t>(info[2]).FromJust();
        auto callback = new Nan::Callback(info[3].As<v8::Function>());

        std::string in_filename = std::string(*param1);


        capa::error_object err;

        AsyncQueueWorker(new Worker_resize_image_IFOB<F>(in_filename,
                                                         width,
                                                         height,
                                                         callback));
    }

    template<bool F(std::string const &,
                    std::string const &,
                    unsigned int, unsigned int,
                    capa::error_object &)>
    void resize_image_IFOF_async(const FunctionCallbackInfo<Value> &info) {
        v8::String::Utf8Value param1(info[0]->ToString());
        v8::String::Utf8Value param2(info[1]->ToString());

        auto width = Nan::To<u_int32_t>(info[2]).FromJust();
        auto height = Nan::To<u_int32_t>(info[3]).FromJust();
        auto callback = new Nan::Callback(info[4].As<v8::Function>());

        std::string  in_filename = std::string(*param1);
        std::string  out_filename = std::string(*param2);


        capa::error_object  err;


        AsyncQueueWorker(new Worker_resize_image_IFOF<F>(in_filename,
                                                         out_filename,
                                                         width,
                                                         height,
                                                         callback));
    }


    template<bool F(std::string const &,
                    std::string const &,
                    unsigned int,
                    unsigned int,
                    unsigned int,
                    capa::error_object &)>
    void create_thumbnail_IFOF(const FunctionCallbackInfo<Value>& info) {

        v8::String::Utf8Value param1(info[0]->ToString());
        v8::String::Utf8Value param2(info[1]->ToString());

        auto orientation = Nan::To<u_int32_t>(info[2]).FromJust();
        auto width = Nan::To<u_int32_t>(info[3]).FromJust();
        auto height = Nan::To<u_int32_t>(info[4]).FromJust();

        std::string  in_filename = std::string(*param1);
        std::string  out_filename = std::string(*param2);
        capa::error_object  err;

        F(in_filename, out_filename, orientation, width, height, err);

        info.GetReturnValue().Set(true);
    }

    template<bool F(std::string const &,
                    std::ostream &,
                    unsigned int,
                    unsigned int, unsigned int,
                    capa::error_object &)>
    void create_thumbnail_IFOB(const FunctionCallbackInfo<Value>& info) {
        v8::String::Utf8Value param1(info[0]->ToString());

        auto orientation = Nan::To<u_int32_t>(info[1]).FromJust();
        auto width = Nan::To<u_int32_t>(info[2]).FromJust();
        auto height = Nan::To<u_int32_t>(info[3]).FromJust();

        std::string  in_filename = std::string(*param1);

        std::ostringstream in_memory(std::ios_base::out | std::ios_base::binary);
        capa::error_object  err;

        F(in_filename, in_memory, orientation, width, height, err);

        auto str_retval = in_memory.str();
        auto retval = Nan::CopyBuffer(str_retval.c_str(),str_retval.size());

        if(!retval.IsEmpty())
        {
            info.GetReturnValue().Set(retval.ToLocalChecked());
        }
    }

    template<bool F(std::istream &,
                    std::ostream &,
                    unsigned int,
                    unsigned int, unsigned int,
                    capa::error_object &)>
    void create_thumbnail_IBOB(const FunctionCallbackInfo<Value>& info) {
        Isolate* isolate = info.GetIsolate();

        Nan::TypedArrayContents<char> data(info[0]);

        auto orientation = Nan::To<u_int32_t>(info[1]).FromJust();
        auto width = Nan::To<u_int32_t>(info[2]).FromJust();
        auto height = Nan::To<u_int32_t>(info[3]).FromJust();

        std::stringstream  in_buffer;
        in_buffer.write(*data, data.length());

        std::ostringstream out_buffer(std::ios_base::in | std::ios_base::out | std::ios_base::binary);
        capa::error_object  err;

        F(in_buffer, out_buffer, orientation, width, height, err);

        if(err.fail()) {
            rapidjson::Document dom;
            auto retval = capa::get_json_text(capa::get_error_json(err, dom, dom.GetAllocator()));
            info.GetReturnValue().Set(String::NewFromUtf8(isolate, retval.c_str()));
        } else{
            auto str_retval = out_buffer.str();
            auto retval = Nan::CopyBuffer(str_retval.c_str(), str_retval.size());

            if(!retval.IsEmpty()) {
                info.GetReturnValue().Set(retval.ToLocalChecked());
            }
        }
    }

    template<bool F(std::string const &,
                    std::string const &,
                    unsigned int,
                    unsigned int,
                    unsigned int,
                    capa::error_object &)>
    void create_thumbnail_IFOF_async(const FunctionCallbackInfo<Value> &info) {
        v8::String::Utf8Value param1(info[0]->ToString());
        v8::String::Utf8Value param2(info[1]->ToString());

        auto orientation = Nan::To<u_int32_t>(info[2]).FromJust();
        auto width = Nan::To<u_int32_t>(info[3]).FromJust();
        auto height = Nan::To<u_int32_t>(info[4]).FromJust();
        auto callback = new Nan::Callback(info[5].As<v8::Function>());

        std::string in_filename = std::string(*param1);
        std::string out_filename = std::string(*param2);


        capa::error_object err;


        AsyncQueueWorker(new Worker_create_thumbnail_IFOF<F>(in_filename,
                                                             out_filename,
                                                             orientation,
                                                             width,
                                                             height,
                                                             callback));
    }

    template <bool F(std::istream &,
                     std::ostream &,
                     unsigned int,
                     unsigned int, unsigned int,
                     capa::error_object &)>
    void create_thumbnail_IBOB_async(const FunctionCallbackInfo<Value>& info) {
        Nan::TypedArrayContents<char> data(info[0]);

        auto orientation = Nan::To<u_int32_t>(info[1]).FromJust();
        auto width = Nan::To<u_int32_t>(info[2]).FromJust();
        auto height = Nan::To<u_int32_t>(info[3]).FromJust();

        auto callback = new Nan::Callback(info[4].As<v8::Function>());

        auto data_length = data.length();
        std::stringstream  in_buffer;
        in_buffer.write(*data, data_length);

        capa::error_object  err;

        if(in_buffer.good()){
            AsyncQueueWorker(new Worker_create_thumbnail_IBOB<F>(in_buffer,
                                                                 orientation,
                                                                 width,
                                                                 height,
                                                                 callback));
        } else {
            err = capa::error_object::create_error(__FUNCTION__,
                                                   "Stream error: ",
                                                   in_buffer.rdstate());
            Isolate* isolate = info.GetIsolate();

            rapidjson::Document dom;
            auto retval = capa::get_json_text(capa::get_error_json(err, dom, dom.GetAllocator()));
            info.GetReturnValue().Set(String::NewFromUtf8(isolate, retval.c_str()));
        }
    }

    template<bool F(std::string const &,
                    std::ostream &,
                    unsigned int,
                    unsigned int, unsigned int,
                    capa::error_object &)>
    void create_thumbnail_IFOB_async(const FunctionCallbackInfo<Value> &info) {
        v8::String::Utf8Value param1(info[0]->ToString());

        auto orientation = Nan::To<u_int32_t>(info[1]).FromJust();
        auto width = Nan::To<u_int32_t>(info[2]).FromJust();
        auto height = Nan::To<u_int32_t>(info[3]).FromJust();
        auto callback = new Nan::Callback(info[4].As<v8::Function>());

        std::string in_filename = std::string(*param1);

        capa::error_object err;

        AsyncQueueWorker(new Worker_create_thumbnail_IFOB<F>(in_filename,
                                                             orientation,
                                                             width,
                                                             height,
                                                             callback));
    }


    void Init(Local<Object> exports) {
        NODE_SET_METHOD(exports, "get_exif", get_exif);
        NODE_SET_METHOD(exports, "extract_exif_jpg_IF", extract_exif_jpg_IF);
        NODE_SET_METHOD(exports, "extract_exif_jpg_IB", extract_exif_jpg_IB);
        NODE_SET_METHOD(exports, "get_picture_info", get_picture_info);

        NODE_SET_METHOD(exports, "resize_image_IFOB", resize_image_IFOB);
        NODE_SET_METHOD(exports, "resize_image_IFOF", resize_image_IFOF);
        NODE_SET_METHOD(exports, "resize_image_IBOB", resize_image_IBOB);

        NODE_SET_METHOD(exports, "resize_image_IFOB_async", resize_image_IFOB_async<capa::resize_image_IFOB>);
        NODE_SET_METHOD(exports, "resize_image_IFOF_async", resize_image_IFOF_async<capa::resize_image_IFOF>);
        NODE_SET_METHOD(exports, "resize_image_IBOB_async", resize_image_IBOB_async<capa::resize_image_IBOB>);

        NODE_SET_METHOD(exports, "create_thumbnail_jpg_IFOF", create_thumbnail_IFOF<capa::create_thumbnail_jpg_IFOF>);
        NODE_SET_METHOD(exports, "create_thumbnail_png_IFOF", create_thumbnail_IFOF<capa::create_thumbnail_png_IFOF>);
        NODE_SET_METHOD(exports, "create_thumbnail_IFOF", create_thumbnail_IFOF<capa::create_thumbnail_IFOF>);
        NODE_SET_METHOD(exports, "create_thumbnail_jpg_IFOF_async",
                        create_thumbnail_IFOF_async<capa::create_thumbnail_jpg_IFOF>);
        NODE_SET_METHOD(exports, "create_thumbnail_png_IFOF_async",
                        create_thumbnail_IFOF_async<capa::create_thumbnail_png_IFOF>);
        NODE_SET_METHOD(exports, "create_thumbnail_IFOF_async",
                        create_thumbnail_IFOF_async<capa::create_thumbnail_IFOF>);

        NODE_SET_METHOD(exports, "create_thumbnail_jpg_IFOB", create_thumbnail_IFOB<capa::create_thumbnail_jpg_IFOB>);
        NODE_SET_METHOD(exports, "create_thumbnail_png_IFOB", create_thumbnail_IFOB<capa::create_thumbnail_png_IFOB>);
        NODE_SET_METHOD(exports, "create_thumbnail_IFOB", create_thumbnail_IFOB<capa::create_thumbnail_IFOB>);
        NODE_SET_METHOD(exports, "create_thumbnail_IFOB_async",
                        create_thumbnail_IFOB_async<capa::create_thumbnail_IFOB>);
        NODE_SET_METHOD(exports, "create_thumbnail_png_IFOB_async",
                        create_thumbnail_IFOB_async<capa::create_thumbnail_png_IFOB>);
        NODE_SET_METHOD(exports, "create_thumbnail_jpg_IFOB_async",
                        create_thumbnail_IFOB_async<capa::create_thumbnail_jpg_IFOB>);

        NODE_SET_METHOD(exports, "create_thumbnail_IBOB", create_thumbnail_IBOB<capa::create_thumbnail_IBOB>);
        NODE_SET_METHOD(exports, "create_thumbnail_jpg_IBOB", create_thumbnail_IBOB<capa::create_thumbnail_jpg_IBOB>);
        NODE_SET_METHOD(exports, "create_thumbnail_png_IBOB", create_thumbnail_IBOB<capa::create_thumbnail_png_IBOB>);
        NODE_SET_METHOD(exports, "create_thumbnail_IBOB_async", create_thumbnail_IBOB_async<capa::create_thumbnail_IBOB>);
        NODE_SET_METHOD(exports, "create_thumbnail_png_IBOB_async", create_thumbnail_IBOB_async<capa::create_thumbnail_png_IBOB>);
        NODE_SET_METHOD(exports, "create_thumbnail_jpg_IBOB_async", create_thumbnail_IBOB_async<capa::create_thumbnail_jpg_IBOB>);
        //NODE_SET_METHOD(exports, "sand_box", sand_box);
    }

    NODE_MODULE(cil, Init)
}
