Capa-Image-Lib
===============

This is a Work In Progress! I'm officially releasing the library but there is still a lot of work to be made  

Capa Image Lib is a C++ wrapper around the GIL and easy-Exif native libraries to enable good and fast picture manipulation.

Feel free to contact me and open issues in the gitlab page.

OS Support
-----------
Currently this library was tested on Ubuntu 17.10+ ,gcc 7 and node v6.5.0.

Also Electron runtime 1.6.2

If you verified it on any other OS feel free to notify me.

File Types Support
-------------------
Currently CIL supports JPEG and PNG file formats.

More to come. 

Dependencies
-----------
apt install cmake

(optional) apt install ninja-build

npm install -g cmake-js

apt install libjpeg-dev 

apt install libpng-dev

API
--------------------------

The module currently knows how to:

* Resize images.
* Extract EXIF (applicable for jpg only).
* Extract image data (width, height).
* Create image thumbnail (rotate image and resize).

The functions that reside in the root exports will identify the image type automatically.

The process of identifying the image type is a waste of time when the file type is already
known, so there is a sub module for directly calling the jpg \ png variants of the functions.

Each functions has 3 Input\Output variants:
* Inputs file from local h.d ----> Outputs file to local h.d. (IFOF).
* Inputs file from local h.d ----> Outputs Buffer (IFOB).
* Inputs Buffer ----> Outputs Buffer (IBOB).
* Currently the option of Inputs Buffer Outputs File is unsupported. 

In the root module the ability to differentiate from input to output in the root module is by:
 * Input type (string or buffer).
 * In the Options object add an output field with the name of the new local file (any other type for 
 output will resolve in buffer output).
 
The simplified interface (using the options object) is only used for the root module all the other functions are using an [old api](https://gitlab.com/Capa_Album/capa_image_lib/blob/master/OldAPI.md)
 
 


Example
-------
```javascript

var cil = require("capa_image_lib");

      const orientation = 1;
        const width = 400;
        const height = 400;
        const input_file = "./test/images/test.jpg";
        const output_file = "./test/images/out.jpg";

        //Create thumb from file
        cil.create_thumbnail(input_file,{ output: output_file,
            orientation,
            width,
            height});
        const left = fs.readFileSync(output_file);
        
        //Create thumb from buffer
        const right  = cil.create_thumbnail(input_file, {
            orientation,
            width,
            height});

        assert.equal(Buffer.compare(left,right),0);
```

Road Map
--------
* Tests
* Add "batch" methods for create_thumbnail_IFOF and resize.
* Add Support for more file types.
* Basic Image tools and filters such as crop, brightness, etc.
* Extract data from images such as channels, histogram, etc.