//
// Created by manicqin on 03/02/18.
//
#include "image_meta_data.h"
#include <boost/algorithm/string.hpp>

#include "boost/gil/extension/io/jpeg.hpp"
#include "boost/gil/extension/io/png.hpp"

namespace capa {

    FILE_TYPES file_type_by_filename(std::string_view file_name) {

        std::size_t pos = file_name.rfind('.') + 1;

        if (pos > 0) {

            auto ext = file_name.substr(pos);

            if (boost::iequals(ext, "jpg")) { return Jpeg; }
            else if (boost::iequals(ext, "png")) { return Png; }
            else { return Undefined; }
        }

        return Undefined;
    }

    bool signature_equal(std::vector<unsigned char> const &file_header, std::vector<unsigned char> const &signature) {
        return std::equal(signature.begin(), signature.end(), file_header.begin());
    }

    FILE_TYPES file_type_by_stream(std::istream &buffer) {

        const int max_header_size = 10;
        std::vector<unsigned char> file_header(max_header_size, 0);
        buffer.readsome(reinterpret_cast<char *>(file_header.data()), max_header_size);
        buffer.seekg(std::ios_base::beg);

        if (signature_equal(file_header, {0xFF, 0xD8, 0xFF, 0xDB})) return Jpeg;
        if (signature_equal(file_header, {0xFF, 0xD8, 0xFF, 0xE0})) return Jpeg;
        if (signature_equal(file_header, {0xFF, 0xD8, 0xFF, 0xE1})) return Jpeg;
        if (signature_equal(file_header, {0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a})) return Png;
        if (signature_equal(file_header, {0x42, 0x4D})) return Bmp;
        if (signature_equal(file_header, {0x49, 0x49, 0x2A, 0x00})) return Tiff;
        if (signature_equal(file_header, {0x4D, 0x4D, 0x00, 0x2A})) return Tiff;

        return Undefined;
    }

    FILE_TYPES file_type_by_template(boost::gil::jpeg_tag) {
        return Jpeg;
    }

    FILE_TYPES file_type_by_template(boost::gil::png_tag) {
        return Png;
    }

    template<typename T>
    FILE_TYPES file_type_by_template(T) {
        return Undefined;
    }
}