//
// Created by manicqin on 26/09/17.
//
#include "capa_image_lib.h"
#include "image_manipulation.h"

namespace capa {

    bool create_thumbnail_jpg_IFOF(std::string const &in_filename,
                                   std::string const &out_filename,
                                   unsigned int orientation,
                                   unsigned int width, unsigned int height,
                                   error_object &out_err) {

        std::ifstream in_buffer;
        in_buffer.open(in_filename, std::ios::binary);
        if (in_buffer.is_open()) {
            std::ofstream out_buffer;
            out_buffer.open(out_filename, std::ios::binary);
            if (out_buffer.is_open()) {
                return create_thumbnail_imp<jpeg_tag, rgb8_image_t>(in_buffer, out_buffer, orientation, width, height,
                                                                    out_err);
            } else {
                out_err = error_object::create_error(__FUNCTION__, {"Failed opening out file:", out_filename},
                                                     out_buffer.rdstate());
            }

        } else {
            out_err = error_object::create_error(__FUNCTION__, {"Failed opening in file:", in_filename},
                                                 in_buffer.rdstate());
        }

        return false;
    }

    bool create_thumbnail_jpg_IFOB(std::string const &in_filename,
                                   std::ostream &out_buffer,
                                   unsigned int orientation,
                                   unsigned int width, unsigned int height,
                                   error_object &out_err) {
        std::ifstream in_buffer;
        in_buffer.open(in_filename, std::ios::binary);
        if (in_buffer.is_open()) {
            return create_thumbnail_imp<jpeg_tag, rgb8_image_t>(in_buffer, out_buffer, orientation, width, height,
                                                                out_err);
        } else {
            out_err = error_object::create_error(__FUNCTION__, {"Failed opening in file:", in_filename},
                                                 in_buffer.rdstate());
        }

        return false;
    }

    bool create_thumbnail_jpg_IBOB(std::istream &in_buffer,
                                   std::ostream &out_buffer,
                                   unsigned int orientation,
                                   unsigned int width, unsigned int height,
                                   error_object &out_err) {
        return create_thumbnail_imp<jpeg_tag, rgb8_image_t>(in_buffer, out_buffer, orientation, width, height, out_err);
    }

    bool create_thumbnail_png_IFOF(std::string const &in_filename,
                                   std::string const &out_filename,
                                   unsigned int orientation,
                                   unsigned int width, unsigned int height,
                                   error_object &out_err) {

        std::ifstream in_buffer;
        in_buffer.open(in_filename, std::ios::binary);
        if (in_buffer.is_open()) {
            std::ofstream out_buffer;
            out_buffer.open(out_filename, std::ios::binary);
            if (out_buffer.is_open()) {
                return create_thumbnail_imp<png_tag, rgba8_image_t>(in_buffer, out_buffer, orientation, width, height,
                                                                    out_err);
            } else {
                out_err = error_object::create_error(__FUNCTION__, {"Failed opening out file:", out_filename},
                                                     out_buffer.rdstate());
            }

        } else {
            out_err = error_object::create_error(__FUNCTION__, {"Failed opening in file:", in_filename},
                                                 in_buffer.rdstate());
        }

        return false;

    }

    bool create_thumbnail_png_IFOB(std::string const &in_filename,
                                   std::ostream &out_buffer,
                                   unsigned int orientation,
                                   unsigned int width, unsigned int height,
                                   error_object &out_err) {

        std::ifstream in_buffer;
        in_buffer.open(in_filename, std::ios::binary);
        if (in_buffer.is_open()) {
            return create_thumbnail_imp<png_tag, rgba8_image_t>(in_buffer, out_buffer, orientation, width, height,
                                                                out_err);
        } else {
            out_err = error_object::create_error(__FUNCTION__, {"Failed opening in file:", in_filename},
                                                 in_buffer.rdstate());
        }

        return false;

    }

    bool create_thumbnail_png_IBOB(std::istream &in_buffer,
                                   std::ostream &out_buffer,
                                   unsigned int orientation,
                                   unsigned int width, unsigned int height,
                                   error_object &out_err) {
        return create_thumbnail_imp<png_tag, rgba8_image_t>(in_buffer, out_buffer, orientation, width, height, out_err);
    }

    bool resize_image_jpg_IFOF(std::string const &in_filename,
                               std::string const &out_filename,
                               unsigned int width, unsigned int height,
                               error_object &out_err) {

        using namespace std;


        ifstream in_buffer(in_filename, ios_base::binary);
        ofstream out_buffer(out_filename, ios_base::binary);

        return resize_image_imp<jpeg_tag, rgb8_image_t>(in_buffer, out_buffer, width, height, out_err);
    }

    bool resize_image_png_IFOF(std::string const &in_filename,
                               std::string const &out_filename,
                               unsigned int width, unsigned int height,
                               error_object &out_err) {
        using namespace std;


        ifstream in_buffer(in_filename, ios_base::binary);
        ofstream out_buffer(out_filename, ios_base::binary);

        return resize_image_imp<png_tag, rgba8_image_t>(in_buffer, out_buffer, width, height, out_err);

    }

    bool resize_image_png_IFOB(std::string const &filename,
                               std::ostream &out_buffer,
                               unsigned int width,
                               unsigned int height,
                               error_object &out_err) {
        std::ifstream in(filename.c_str(), std::ios::binary);

        return resize_image_imp<png_tag, rgba8_image_t>(in, out_buffer, width, height, out_err);
    }

    bool resize_image_jpg_IFOB(std::string const &filename,
                               std::ostream &out_buffer,
                               unsigned int width,
                               unsigned int height,
                               error_object &out_err) {
        std::ifstream in(filename.c_str(), std::ios::binary);

        return resize_image_imp<jpeg_tag, rgb8_image_t>(in, out_buffer, width, height, out_err);
    }

    bool resize_image_png_IBOB(std::istream &in_buffer,
                               std::ostream &out_buffer,
                               unsigned int width,
                               unsigned int height,
                               error_object &out_err) {
        return resize_image_imp<png_tag, rgba8_image_t>(in_buffer, out_buffer, width, height, out_err);
    }

    bool resize_image_jpg_IBOB(std::istream &in_buffer,
                               std::ostream &out_buffer,
                               unsigned int width,
                               unsigned int height,
                               error_object &out_err) {
        return resize_image_imp<jpeg_tag, rgb8_image_t>(in_buffer, out_buffer, width, height, out_err);
    }
    //General manipulators

    bool resize_image_IFOF(std::string const &in_filename,
                           std::string const &out_filename,
                           unsigned int width, unsigned int height,
                           error_object &out_err) {

        switch (file_type_by_filename(in_filename)) {
            case FILE_TYPES::Jpeg: {


                return resize_image_jpg_IFOF(in_filename,
                                             out_filename,
                                             width,
                                             height,
                                             out_err);
                break;
            }
            case FILE_TYPES::Png: {

                return resize_image_png_IFOF(in_filename,
                                             out_filename,
                                             width,
                                             height,
                                             out_err);
                break;
            }
        }

        return false;

    }

    bool resize_image_IFOB(std::string const &in_filename,
                           std::ostream &out_buffer,
                           unsigned int width, unsigned int height,
                           error_object &out_err) {

        switch (file_type_by_filename(in_filename)) {
            case FILE_TYPES::Jpeg: {


                return resize_image_jpg_IFOB(in_filename,
                                             out_buffer,
                                             width, height,
                                             out_err);
                break;
            }
            case FILE_TYPES::Png: {

                return resize_image_png_IFOB(in_filename,
                                             out_buffer,
                                             width, height,
                                             out_err);
                break;
            }
        }

        out_err = error_object::create_error(__FUNCTION__, {"Wrong file type:", in_filename}, 0);

        return false;

    }

    bool resize_image_IBOB(std::istream &in_buffer,
                           std::ostream &out_buffer,
                           unsigned int width, unsigned int height,
                           error_object &out_err) {

        switch (file_type_by_stream(in_buffer)) {
            case FILE_TYPES::Jpeg: {


                return resize_image_jpg_IBOB(in_buffer,
                                             out_buffer,
                                             width, height,
                                             out_err);
                break;
            }
            case FILE_TYPES::Png: {

                return resize_image_png_IBOB(in_buffer,
                                             out_buffer,
                                             width, height,
                                             out_err);
                break;
            }
        }

        const int max_sample_size = 50;
        std::string file_header(max_sample_size, ' ');
        in_buffer.readsome(file_header.data(), max_sample_size);
        std::stringstream buf;
        for (int idx = 0; idx < max_sample_size; idx++) {
            buf << std::hex << file_header[idx] << " ";
        }

        out_err = error_object::create_error(__FUNCTION__, {"Wrong file type - Sample:", buf.str()}, -1);

        return false;

    }

    bool create_thumbnail_IFOB(std::string const &in_filename,
                               std::ostream &out_buffer,
                               unsigned int orientation,
                               unsigned int width, unsigned int height,
                               error_object &out_err) {

        switch (file_type_by_filename(in_filename)) {
            case FILE_TYPES::Jpeg: {

                return create_thumbnail_jpg_IFOB(in_filename,
                                                 out_buffer,
                                                 orientation,
                                                 width, height,
                                                 out_err);
                break;
            }
            case FILE_TYPES::Png: {

                return create_thumbnail_png_IFOB(in_filename,
                                                 out_buffer,
                                                 orientation,
                                                 width, height,
                                                 out_err);
                break;
            }
        }

        out_err = error_object::create_error(__FUNCTION__, {"Wrong file type:", in_filename}, 0);
        return false;
    }

    bool create_thumbnail_IBOB(std::istream &in_buffer,
                               std::ostream &out_buffer,
                               unsigned int orientation,
                               unsigned int width, unsigned int height,
                               error_object &out_err) {

        auto file_type = file_type_by_stream(in_buffer);
        switch (file_type) {
            case FILE_TYPES::Jpeg: {

                return create_thumbnail_jpg_IBOB(in_buffer,
                                                 out_buffer,
                                                 orientation,
                                                 width,
                                                 height,
                                                 out_err);
                break;
            }
            case FILE_TYPES::Png: {

                return create_thumbnail_png_IBOB(in_buffer,
                                                 out_buffer,
                                                 orientation,
                                                 width,
                                                 height,
                                                 out_err);
                break;
            }
        }

        const int max_sample_size = 50;
        std::string file_header(max_sample_size, ' ');
        in_buffer.readsome(file_header.data(), max_sample_size);
        std::stringstream buf;
        for (int idx = 0; idx < max_sample_size; idx++) {
            buf << std::hex << file_header[idx] << " ";
        }

        out_err = error_object::create_error(__FUNCTION__, {"Wrong file type - Sample:", buf.str()}, -1);
        return false;
    }

    bool create_thumbnail_IFOF(std::string const &in_filename,
                               std::string const &out_filename,
                               unsigned int orientation,
                               unsigned int width, unsigned int height,
                               error_object &out_err) {
        switch (file_type_by_filename(in_filename)) {
            case FILE_TYPES::Jpeg: {


                return create_thumbnail_jpg_IFOF(in_filename,
                                                 out_filename,
                                                 orientation,
                                                 width, height,
                                                 out_err);
                break;
            }
            case FILE_TYPES::Png: {

                return create_thumbnail_png_IFOF(in_filename,
                                                 out_filename,
                                                 orientation,
                                                 width, height,
                                                 out_err);
                break;
            }
        }

        out_err = error_object::create_error(__FUNCTION__, {"Wrong file type:", in_filename}, 0);

        return false;
    }


    int read_picture_IB(std::istream &in_buffer, easyexif::EXIFInfo &result) {
        std::string s(std::istreambuf_iterator<char>(in_buffer), {});

        return result.parseFrom(s);
    }

    int read_picture_IF(std::string_view file_path, easyexif::EXIFInfo &result) {
        return parse_exif_from_file(file_path.data(), result);
    }

    void
    extract_exif_jpg_IF(rapidjson::Value const &file_names, rapidjson::Value &doc,
                        rapidjson::Document::AllocatorType &alloc) {
        if (!doc.IsArray())
            doc.SetArray();

        auto arr = file_names.GetArray();

        for (auto iter = arr.Begin(); iter != arr.End(); ++iter) {
            std::string file_name = iter->GetString();
            rapidjson::Value pic;
            easyexif::EXIFInfo exif;
            auto ret = read_picture_IF(file_name, exif);
            if (ret == 0) {
                get_json(exif, pic, alloc);
                pic.AddMember("Filename", file_name, alloc);
                doc.PushBack(pic, alloc);
            }
        }
    }

    void extract_exif_jpg_IF(std::vector<std::string> const &file_names, rapidjson::Value &doc,
                             rapidjson::Document::AllocatorType &alloc) {
        if (!doc.IsArray())
            doc.SetArray();

        for (auto &file_name: file_names) {
            rapidjson::Value pic;
            easyexif::EXIFInfo exif;
            auto ret = read_picture_IF(file_name, exif);
            if (ret == 0) {
                get_json(exif, pic, alloc);
                pic.AddMember("Filename", file_name, alloc);
                doc.PushBack(pic, alloc);
            }
        }
    }

    void
    extract_exif_jpg_IF(std::string_view file_name, rapidjson::Value &doc, rapidjson::Document::AllocatorType &alloc) {
        if (!doc.IsObject())
            doc.SetObject();

        rapidjson::Value pic;
        easyexif::EXIFInfo exif;

        auto ret = read_picture_IF(file_name, exif);
        if (ret == 0) {
            get_json(exif, doc, alloc);
        }
    }

    void
    extract_exif_jpg_IB(std::istream &in_buffer, rapidjson::Value &doc, rapidjson::Document::AllocatorType &alloc) {
        if (!doc.IsObject())
            doc.SetObject();

        rapidjson::Value pic;
        easyexif::EXIFInfo exif;
        std::string s(std::istreambuf_iterator<char>(in_buffer), {});
        auto ret = exif.parseFrom(s);

        if (ret == 0) {
            get_json(exif, doc, alloc);
        }
    }

    bool extract_picture_info(std::string_view in_filename, rapidjson::Value &doc,
                              rapidjson::Document::AllocatorType &alloc) {

        if (!doc.IsObject())
            doc.SetObject();

        int64_t width = 0;
        int64_t height = 0;
        switch (file_type_by_filename(in_filename)) {
            case FILE_TYPES::Jpeg: {

                rgb8_image_t img;
                read_image(in_filename.data(), img, jpeg_tag());
                width = img.width();
                height = img.height();

                break;
            }
            case FILE_TYPES::Png: {

                rgba8_image_t img;
                read_image(in_filename.data(), img, png_tag());
                width = img.width();
                height = img.height();

                break;
            }
        }

        if (width > 0 && height > 0) {
            using namespace rapidjson;

            Value obj(rapidjson::kObjectType);

            obj.AddMember("ImageWidth",
                          Value(kNumberType).SetUint64(width),
                          alloc);

            obj.AddMember("ImageHeight",
                          Value(kNumberType).SetUint64(height),
                          alloc);

            obj.AddMember("Filename",
                          std::string(in_filename.data()),
                          alloc);

            doc.AddMember("EXIF", obj.GetObject(), alloc);
            return true;
        }

        return false;
    }

    void extract_picture_info(rapidjson::Value const &file_names, rapidjson::Value &doc,
                              rapidjson::Document::AllocatorType &alloc) {
        if (!doc.IsArray())
            doc.SetArray();

        auto arr = file_names.GetArray();

        for (auto iter = arr.Begin(); iter != arr.End(); ++iter) {
            rapidjson::Value obj(rapidjson::kObjectType);
            if (extract_picture_info(iter->GetString(), obj, alloc))
                doc.PushBack(obj, alloc);

        }
    }

    void extract_picture_info(std::vector<std::string> const &file_names, rapidjson::Value &doc,
                              rapidjson::Document::AllocatorType &alloc) {
        if (!doc.IsArray())
            doc.SetArray();

        for (auto &file_name: file_names) {
            rapidjson::Value pic;

            if (extract_picture_info(file_name, pic, alloc)) {
                doc.PushBack(pic, alloc);
            }
        }

    }


}