/**
 * Created by manicqin on 12/05/17.
 */

const cil = require('../index.js');
const fs = require('fs');
const assert  = require('assert');

describe('testing the methods of images details extraction', function() {
    const source_arr = ["./test/images/test.jpg",
        "./test/images/test.jpg",
        "./test/images/test.jpg"];

    const out_get_picture_info = cil.get_picture_info(source_arr);
    const out_get_exif = cil.get_exif(source_arr);

    it('get_picture_info - The number of images should be equal to the output', function() {
        assert.equal(out_get_picture_info.length, source_arr.length);
    });

    //console.log(source_arr.length, out_get_exif.length)
    it('get_exif - The number of images should be 1 more than to the output, one of the images has a corrupted EXIF', function() {
        assert.equal(source_arr.length - out_get_exif.length, 0);
    });
});

describe('comparing the methods of images details extraction', function() {
    const source_arr = ["./test/images/test.jpg"];

    const out_get_picture_info = cil.get_picture_info(source_arr);
    const out_get_exif = cil.get_exif(source_arr);
    it('testing the output of get_exif vs get_picture_info', function() {
        assert.equal(out_get_picture_info[0].EXIF.ImageWidth, out_get_exif[0].EXIF.ImageWidth);
        assert.equal(out_get_picture_info[0].EXIF.ImageHeight, out_get_exif[0].EXIF.ImageHeight);
    });
});

describe('create_thumbnail_jpg_IFOB', function() {
    it('Should be the same as the create_thumbnail_jpg_IFOF', function() {
        const orientation = 1;
        const width = 400;
        const height = 400;
        const intput_file = "./test/images/test.jpg";
        const output_file = "./test/images/out.jpg";

        cil.create_thumbnail(intput_file, {
            output: output_file,
            orientation,
            width,
            height
        });
        const left = fs.readFileSync(output_file);
        const right = cil.create_thumbnail(intput_file, {
            orientation,
            width,
            height
        });

        assert.equal(Buffer.compare(left, right), 0);
    });
});

describe('create_thumbnail_jpg_IBOB', function() {
    it('Should be the same as the create_thumbnail_jpg_IFOB', function() {
        const orientation = 1;
        const width = 400;
        const height = 400;
        const filename = "./test/images/test.jpg";
        const buffer = fs.readFileSync(filename);

        const right = cil.create_thumbnail(buffer, {
            orientation,
            width,
            height
        });

        //fs.writeFileSync("./test/out_images/create_thumbnail_jpg_IBOB.jpg",right);
        const left = cil.create_thumbnail(filename, {
            orientation,
            width,
            height
        });

        //fs.writeFileSync("./test/out_images/create_thumbnail_jpg_IFOB.jpg",left);
        if(!Buffer.isBuffer(left)){
            console.log(left);
            assert.fail("create_thumbnail_jpg_IFOB");
        }
        else if(!Buffer.isBuffer(right)){
            console.log(right);
            assert.fail("create_thumbnail_jpg_IBOB");
        }
        else{
            assert.equal(Buffer.compare(left, right), 0);
        }
    });
});

describe('create_thumbnail_png_IBOB', function() {
    it('Should be the same as the create_thumbnail_png_IFOB', function() {
        const orientation = 1;
        const width = 400;
        const height = 400;
        const input_file = "./test/images/test.png";
        const buffer = fs.readFileSync(input_file);

        const right = cil.create_thumbnail(buffer, {
            orientation,
            width,
            height
        });

        //fs.writeFileSync("./test/out_images/create_thumbnail_IBOB.png",right);

        const left = cil.create_thumbnail(input_file, {
            orientation,
            width,
            height
        });

        //fs.writeFileSync("./test/out_images/create_thumbnail_IFOB.png",left);

        if(!Buffer.isBuffer(left)){
            console.log(left);
            assert.fail("create_thumbnail_png_IFOB");
        }
        else if(!Buffer.isBuffer(right)){
            console.log(right);
            assert.fail("create_thumbnail_png_IBOB");

        }
        else{
            assert.equal(Buffer.compare(left, right), 0);
        }
    })
});

describe('create_thumbnail_png_IFOB', function() {
    it('Should be the same as the create_thumbnail_png_IFOF', function() {
        const orientation = 2;
        const width = 400;
        const height = 400;
        const input_file = "./test/images/test.png";
        cil.create_thumbnail(input_file, {
            output: "./test/out_images/create_thumb_buffer_png_out_1.png",
            orientation,
            width,
            height
        });
        const left = fs.readFileSync("./test/out_images/create_thumb_buffer_png_out_1.png");
        const right = cil.create_thumbnail(input_file, {
            orientation,
            width,
            height
        });

        //fs.writeFileSync("./test/out_images/create_thumb_buffer_png_out_2.png",right);

        if(!Buffer.isBuffer(left)){
            console.log(left);
        }
        else if(!Buffer.isBuffer(left)){
            console.log(right);
        }
        else{
            assert.equal(Buffer.compare(left, right), 0);
        }

    })
});

describe('create_thumbnail_jpg_IFOB', function() {
    it('Should be the same as the create_thumbnail_jpg_IFOF', function() {
        const orientation = 2;
        const width = 400;
        const height = 400;
        const input_filename = "./test/images/test.jpg";
        const output = "./test/out_images/create_thumb_buffer_jpg_out_1.jpg";
        cil.create_thumbnail(input_filename, {
            output,
            orientation,
            width,
            height
        });
        const left = fs.readFileSync(output);
        const right = cil.create_thumbnail(input_filename, {
            orientation,
            width,
            height
        });

        //fs.writeFileSync("./test/out_images/create_thumb_buffer_jpg_out_2.jpg",right);

        if(!Buffer.isBuffer(left)){
            console.log(left);
        }
        else if(!Buffer.isBuffer(left)){
            console.log(right);
        }
        else{
            assert.equal(Buffer.compare(left, right), 0);
        }

    })
});

describe('Resize - Testing if the resize succeeded by comparing the return val and the resize file name', function() {
    it('Resize resize_buffer and resize by comparing the buffers', function() {

        const width = 400;
        const height = 400;
        const input_file = "./test/images/test.jpg";
        const output_file = "./test/out_images/resize_from_jpeg.png";

        const out_buffer = cil.resize_image(input_file, {
            width,
            height
        });

        cil.resize_image(input_file, {
            output: output_file,
            width,
            height
        });

        const out_file = fs.readFileSync(output_file);

        assert.equal(Buffer.compare(out_buffer, out_file), 0);
    })
});

// describe('BLOB Interface', function() {
//     it('Resize BLOB Buffer', function() {
//
//         const width = 400;
//         const height = 400;
//
//         const buffer = fs.readFileSync("./test/images/test.jpg");
//         let blob_in = new Blob([buffer], {type: 'application/octet-stream'});
//
//         const right  = cil.resize_image(blob_in,
//             width,
//             height);
//     })
// });
