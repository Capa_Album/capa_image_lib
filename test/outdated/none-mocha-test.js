const cil = require('../../index.js');
const fs = require('fs');
const assert = require('assert');


function test_buffer(obj_a, obj_b){

    if (!Buffer.isBuffer(obj_a)) {
        console.log(obj_a);
        assert.fail("obj_a not buffer")
    }
    else if (!Buffer.isBuffer(obj_b)) {
        console.log(obj_b);
        assert.fail("obj_b not buffer")
    }
    else {
        console.log(Buffer.compare(left, right))
    }
}

function a(flag) {

    const orientation = 1
    const width = 400
    const height = 400

    const buffer = fs.readFileSync("../test/images/test.jpg", flag)

    const right = cil.create_thumbnail_IBOB(buffer,
        orientation,
        width,
        height)

    const left = cil.create_thumbnail_IFOB("../test/images/test.jpg",
        orientation,
        width,
        height)

    test_buffer(right, left)
}

function b(flag) {

    const orientation = 1
    const width = 400
    const height = 400

    const buffer = fs.readFileSync("/home/manicqin/Projects/capa_image_lib/test/images/test.png", flag)

    //console.log(buffer);
    cil.create_thumbnail_png_IBOB_async(buffer,
        orientation,
        width,
        height)
        .then(val => console.log(val, "success"))
        .catch(err => console.log(err, "error"));

    //console.log(buffer);
    cil.create_thumbnail_png_IFOB_async(
        "/home/manicqin/Projects/capa_image_lib/test/images/test.png",
        orientation,
        width,
        height)
        .then(val => console.log(val, "success"))
        .catch(err => console.log(err, "error"));


    cil.create_thumbnail_png_IFOF_async(
        "/home/manicqin/Projects/capa_image_lib/test/images/test.png",
        "/home/manicqin/Projects/capa_image_lib/test/images/test_async.png",
        orientation,
        width,
        height)
        .then(val => console.log(val, "success"))
        .catch(err => console.log(err, "error"));
}

function runner(flag){

    //console.log(flag)
    // try {
    //     a(flag)
    // }
    // catch (e) {
    //
    // }

    try {
        b(flag)
    }
    catch (e) {
        console.log("4", e);
    }
}

runner()
// runner("ascii")
// runner("binary")
// runner("base64")
// runner("utf8")
// runner("hex")