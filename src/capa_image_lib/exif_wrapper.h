//
// Created by manicqin on 23/10/17.
//

#ifndef CIL_EXIF_WRAPPER_H
#define CIL_EXIF_WRAPPER_H

#include <string>

#define RAPIDJSON_HAS_STDSTRING 1
#define RAPIDJSON_HAS_CXX11_RANGE_FOR 1

#include "exif.h"
#include "rapidjson/document.h"

int parse_exif_from_file(std::string const file_path, easyexif::EXIFInfo &result);

void get_json(easyexif::EXIFInfo::LensInfo_t const& lens, rapidjson::Value &obj, rapidjson::Document::AllocatorType & alloc);

void get_json(easyexif::EXIFInfo::Geolocation_t const& geo, rapidjson::Value &obj, rapidjson::Document::AllocatorType & alloc);

void get_json(easyexif::EXIFInfo::Geolocation_t::Coord_t const& coords, rapidjson::Value &obj, rapidjson::Document::AllocatorType &alloc);

void get_json(easyexif::EXIFInfo const& exifInfo, rapidjson::Value& doc, rapidjson::Document::AllocatorType  & allocator);

#endif //CIL_EXIF_WRAPPER_H
