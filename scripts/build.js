const fs = require('fs');

const source = `${__dirname}/../build/Debug`;
const destination = `${__dirname}/../cmake-build-debuggingnode`;
const filename = 'cil_node.node';

console.log('Version: ' + process.version);
console.log(source);

if (!fs.existsSync(source)) {
    fs.mkdirSync(source);
}

if (!fs.existsSync(destination)) {
    fs.mkdirSync(destination);
}


fs.link(`${source}/${filename}`, `${destination}/${filename}`);