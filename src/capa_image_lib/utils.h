//ostringstreamstd
// Created by manicqin on 30/10/17.
//

#ifndef CIL_UTILS_H
#define CIL_UTILS_H

#define RAPIDJSON_HAS_STDSTRING 1
#define RAPIDJSON_HAS_CXX11_RANGE_FOR 1
#define RAPIDJSON_HAS_CXX11_RVALUE_REFS 1

#include <string>
#include <vector>

#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string.hpp>

#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>

namespace capa {

    class error_object {
      public:

        error_object(std::string const &func, std::string const &desc, int code) :
            function_name(func), err_description(desc), err_code(code), is_err(true) {}

        error_object() :
            function_name(""), err_description(""), err_code(0), is_err(false) {}

        bool fail() const { return is_err; }

        bool success() const { return !is_err; }

        static error_object create_error(std::string const &func, std::string const &desc, int code) {
            return error_object(func, desc, code);
        }

        static error_object create_error(std::string const &func, std::vector<std::string> &&desc, int code) {
            return error_object(func, boost::join(desc, " "), code);
        }

        static error_object create_success() {
            return error_object();
        }

        std::string function_name;
        std::string err_description;
        int err_code;
        bool is_err;
    };

    rapidjson::Value &
    get_error_json(error_object const &in_err, rapidjson::Value &obj, rapidjson::Document::AllocatorType &alloc);

    std::string get_json_text(rapidjson::Value const &in_doc);
}
#endif //CIL_UTILS_H
